<?
namespace app\models;

use yii\db\ActiveRecord;

class MoradoresModel extends ActiveRecord{

    public static function tableName(){
        return 'ap_morador';
    }

    public function rules(){
        return[
            [['id_unidade','id_bloco','id_condominio','nome','cpf','nascimento','email','telefone'],'required'],
        ];
    }
}

?>