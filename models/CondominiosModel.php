<?
namespace app\models;

use yii\db\ActiveRecord;

class CondominiosModel extends ActiveRecord{

    public static function tableName(){
        return 'ap_condominio';
    }

    public function rules(){
        return[
            [['id_administradora','nome','qtBlocos','cep','logradouro','numero','bairro','cidade','estado'],'required']
        ];
        
    }
}

?>