<?
namespace app\models;

use yii\db\ActiveRecord;

class ConselhoModel extends ActiveRecord{

    public static function tableName(){
        return 'ap_conselho';
    }

    public function rules(){
        return[
            [['id_condominio','funcao','nomeConselho','cpf','email','telefone'],'required']
        ];
        
    }
}

?>