<?
namespace app\models;

use yii\db\ActiveRecord;

class AdministradorasModel extends ActiveRecord{

    public static function tableName(){
        return 'ap_administradora';
    }

    public function rules(){
        return[
            [['nome','cnpj'], 'required']
        ];
    }

}


?>