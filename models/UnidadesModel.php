<?
namespace app\models;

use yii\db\ActiveRecord;

class UnidadesModel extends ActiveRecord{
    
    public static function tableName(){
        return 'ap_unidade';
    }

    public function rules(){
        return[
            [['id_bloco','id_condominio','numeroUnidade','metragem','vagasDeGaragem'],'required']
        ];
    }

}



?>