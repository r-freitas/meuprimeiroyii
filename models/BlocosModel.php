<?
namespace app\models;

use yii\db\ActiveRecord;

class BlocosModel extends ActiveRecord{

    public static function tableName(){
        return 'ap_bloco';
    }

    public function rules(){
        return[
            [['id_condominio','nome','numeroAndares','unidadesPAndar',], 'required']
        ];
    }
}

?>