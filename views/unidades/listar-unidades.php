<?

use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\modalComponent;
use app\components\alertComponent;

$url_site = Url::base(true);
$this->title = "Listar Unidades";
if(isset($_GET['myAlert'])){
    echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg']);
}
?>
<h1>Unidades</h1>
<div class="row">
    <table class="table col-12 table-responsive-lg mb-5 mt-5 tabelaUnidades">
        <thead>
            <tr>
                <th scope="col">Número Unidade</th>
                <th scope="col">Metragem</th>
                <th scope="col">Vagas de Garagem</th>
                <th scope="col">Condominio</th>
                <th scope="col">Bloco</th>
                <th scope="col">Data Cad.</th>
                <th><a href="<?= $url_site ?>/index.php?r=unidades%2Fcadastrar-unidades"><button class="btn btn-dark">Adicionar</button></a></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach($unidades as $valor){ 
            ?>
                <tr data-id="<?= $valor['id']; ?>" class="unidade">
                    <td><?= $valor['numeroUnidade'] ?></td>
                    <td><?= $valor['metragem'] ?></td>
                    <td><?= $valor['vagasDeGaragem'] ?></td>
                    <td><?= $valor['nomeCondo'] ?></td>
                    <td><?= $valor['nomeBloco'] ?></td>
                    <td><?= Yii::$app->formatter->format($valor['dataCadastro'], 'date') ?></td>
                    <td><a class="p-1 removerUnidade" href="<?= $url_site ?>/index.php?r=unidades/deleta-unidade&id=<?= $valor['id'] ?>"><i class="bi bi-x-circle-fill text-danger"></i></a><a class="p-1 openModal" href="<?= $url_site ?>/index.php?r=unidades/editar-unidade&id=<?= $valor['id'] ?>"><i class="bi bi-pencil-square ml-1 text-info"></i></a></td>
                </tr>
    
            <?php } ?> 
            <tr>
                <td colspan="6">&nbsp;</td>
                <td class="totalRegistros"><?php echo 'Registros: ' . ($paginacao->totalCount<10? '0'. $paginacao->totalCount : $paginacao->totalCount)?></td>
            </tr>
        </tbody>
    </table>
</div>

    <?= LinkPager::widget(
    [
        'pagination' => $paginacao,
        'linkContainerOptions' => [
            'class' => 'btn-group'
        ],
        'linkOptions' => [
            'class' => 'btn btn-dark'
        ],
        'disabledListItemSubTagOptions' => [
            'class' => 'btn btn-secondary'
        ]
    ]
    ) ?>
    <?= modalComponent::initModal('Edição de Unidade'); ?>