<?
use app\controllers\CondominiosController;
use app\controllers\BlocosController;
use yii\helpers\Html;
use yii\helpers\Url;
use app\controllers\UnidadesController;
$url_site = Url::base($schema = true);
$this->title = "Cadastrar Unidades";
?>
<h5>Cadastro de Unidade</h5>
<form action="<?= Url::to(['unidades/realiza-cadastro-unidade']) ?>" method="post" class="col-12 mt-3 formUnidade">
    <div class="form-group">
        <label for="id_condominio">Condomínio</label>
        <select class="custom-select" name="id_condominio" id="id_condominio">
            <option value="">Condominio</option>
            <?php
            foreach (CondominiosController::listaCondominioSelect() as $opcaoCond) { ?>
                <option value="<?= $opcaoCond['id'] ?>"><?= $opcaoCond['nome'] ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group">
        <label for="id_bloco">Bloco</label>
        <select class="custom-select fromBloco" name="id_bloco" id="id_bloco">

        </select>
    </div>
    <div class="form-group">
        <label for="numeroUnidade">Número da unidade</label>
        <input type="text" class="form-control" id="numeroUnidade" name="numeroUnidade" value="" required>
    </div>
    <div class="form-group">
        <label for="metragem">Metragem da unidade</label>
        <input type="text" class="form-control" id="metragem" name="metragem" value="" required>
    </div>
    <div class="form-group">
        <label for="vagasDeGaragem">Vagas de garagem</label>
        <input type="text" class="form-control" id="vagasDeGaragem" name="vagasDeGaragem" value="" required>
    </div>
    <input type="hidden" name="<?= \yii::$app->request->csrfParam ?>" value="<?= \yii::$app->request->csrfToken ?>">
    <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
</form>