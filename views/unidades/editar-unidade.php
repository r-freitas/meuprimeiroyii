<?

use app\components\selectedComponent;
use app\controllers\CondominiosController;
use app\controllers\BlocosController;
use yii\helpers\Html;
use yii\helpers\Url;
use app\controllers\UnidadesController;

$url_site = Url::base(true);

?>

<form action="<?= Url::to(['unidades/realiza-edicao-unidade']) ?>" method="post" class="col-12 mt-3 formUnidade">
    <div class="form-group">
        <label for="id_condominio">Condomínio</label>
        <select class="custom-select" name="id_condominio" id="id_condominio">
            <option value="">Condominio</option>
            <?php
            foreach (CondominiosController::listaCondominioSelect() as $opcaoCond) { ?>
                <option value="<?= $opcaoCond['id'] ?>" <?= selectedComponent::isSelected($opcaoCond['id'], $edit['id_condominio']) ?>><?= $opcaoCond['nome'] ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group">
        <label for="id_bloco">Bloco</label>
        <select class="custom-select fromBloco" name="id_bloco" id="id_bloco">
            <option value="">Bloco</option>
            <?php
            foreach (BlocosController::listaBlocoEditar($edit['id_condominio']) as $bloco) { ?>
                <option value="<?= $bloco['id'] ?>" <?= selectedComponent::isSelected($bloco['id'], $edit['id_bloco']) ?>><?= $bloco['nome'] ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group">
        <label for="numeroUnidade">Número da unidade</label>
        <input type="text" class="form-control" id="numeroUnidade" name="numeroUnidade" value="<?= $edit['numeroUnidade'] ?>" required>
    </div>
    <div class="form-group">
        <label for="metragem">Metragem da unidade</label>
        <input type="text" class="form-control" id="metragem" name="metragem" value="<?= $edit['metragem'] ?>" required>
    </div>
    <div class="form-group">
        <label for="vagasDeGaragem">Vagas de garagem</label>
        <input type="text" class="form-control" id="vagasDeGaragem" name="vagasDeGaragem" value="<?= $edit['vagasDeGaragem'] ?>" required>
    </div>
    <input type="hidden" name="<?= \yii::$app->request->csrfParam ?>" value="<?= \yii::$app->request->csrfToken ?>">
    <input type="hidden" name="id" value="<?= $edit['id'] ?>">
    <div class="row justify-content-between">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
    </div>
</form>