<?

use app\components\modalComponent;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\alertComponent;
$this->title = "Listar Blocos";
$url_site = Url::base(true);

if(isset($_GET['myAlert'])){
    echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg']);
}
?>
<h1>Blocos</h1>
<div class="row">
    <table class="table col-12 table-responsive-lg mb-5 mt-5 tabelaBlocos">
        <thead>
            <tr>
                <th scope="col">Nome bloco</th>
                <th scope="col">N° Andares</th>
                <th scope="col">Unidades Por Andar</th>
                <th scope="col">Condomínio</th>
                <th scope="col">Data Cad.</th>
                <th><a href="<?= $url_site ?>/index.php?r=blocos%2Fcadastrar-blocos"><button class="btn btn-dark">Adicionar</button></a></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach($blocos as $bloco){ ?>
                <tr data-id="<?= $bloco['id']; ?>" class="bloco">
                    <td><?= $bloco['nome'] ?></td>
                    <td><?= $bloco['numeroAndares'] ?></td>
                    <td><?= $bloco['unidadesPAndar'] ?></td>
                    <td><?= $bloco['nomeCond'] ?></td>
                    <td><?= Yii::$app->formatter->format($bloco['dataCadastro'],'date') ?></td>
                    <td><a class="p-1 removerBloco" href="<?= $url_site ?>/index.php?r=blocos/deleta-bloco&id=<?= $bloco['id']; ?>"><i class="bi bi-x-circle-fill text-danger"></i></a><a class="p-1 openModal" href="<?= $url_site ?>/index.php?r=blocos/editar-bloco&id=<?= $bloco['id']; ?>"><i class="bi bi-pencil-square ml-1 text-info"></i></a></td>
                </tr>
    
            <?php } ?> 
            <tr>
                <td colspan="5">&nbsp;</td>
                <td class="totalRegistros"><?php echo 'Registros: ' . ($paginacao->totalCount<10? '0'. $paginacao->totalCount : $paginacao->totalCount)?></td>
            </tr>
        </tbody>
    </table>
</div>

<?= LinkPager::widget(
    [
        'pagination' => $paginacao,
        'linkContainerOptions' => [
            'class' => 'btn-group'
        ],
        'linkOptions' => [
            'class' => 'btn btn-dark'
        ],
        'disabledListItemSubTagOptions' => [
            'class' => 'btn btn-secondary'
        ]
    ]
    ) ?>
<?= modalComponent::initModal('Edição de Bloco') ?>