<?

use app\components\selectedComponent;
use yii\helpers\Url;
use app\controllers\CondominiosController;
?>

<form action="<?= Url::to(['blocos/realiza-edicao-bloco']) ?>" method="post" class="col-12 mt-3 formBloco">
    <div class="form-group">
        <label for="condominio">Condomínio</label>
        <select class="custom-select" name="id_condominio">
            <option value="">Condominio</option>
            <?php
            foreach (CondominiosController::listaCondominioSelect() as $opcaoCond) { ?>
                <option value="<?= $opcaoCond['id'] ?>" <?= selectedComponent::isSelected($opcaoCond['id'], $edit['id_condominio']) ?>><?= $opcaoCond['nome'] ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group">
        <label for="nomeBloco">Nome do Bloco</label>
        <input type="text" class="form-control" id="nome" name="nome" value="<?= $edit['nome'] ?>" required>
    </div>
    <div class="form-group">
        <label for="numeroAndares">Número de Andares</label>
        <input type="text" class="form-control" id="numeroAndares" name="numeroAndares" value="<?= $edit['numeroAndares'] ?>" required>
    </div>
    <div class="form-group">
        <label for="unidadesPAndar">Unidades por Andar</label>
        <input type="text" class="form-control" id="unidadesPAndar" name="unidadesPAndar" value="<?= $edit['unidadesPAndar'] ?>" required>
    </div>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam ?>" value="<?= \yii::$app->request->csrfToken ?>">
    <input type="hidden" name="id" value="<?= $edit['id'] ?>">
    <div class="row justify-content-between">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
    </div>
</form>