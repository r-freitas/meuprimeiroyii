<?
use yii\helpers\Html;
use yii\helpers\Url;
use app\controllers\CondominiosController;
$this->title = "Cadastrar Blocos";
?>

<h5>Cadastro de Bloco</h5>
    <form action="<?= Url::to(['blocos/realiza-cadastro-bloco']) ?>" method="post" class="col-12 mt-3 formBloco">
        <div class="form-group">
            <label for="condominio">Condomínio</label>
            <select class="custom-select" name="id_condominio">
                <option value="">Condominio</option>
                <?php 
                foreach(CondominiosController::listaCondominioSelect() as $opcaoCond){ ?>
                        <option value="<?= $opcaoCond['id'] ?>"><?= $opcaoCond['nome'] ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="nomeBloco">Nome do Bloco</label>
            <input type="text" class="form-control" id="nome" name="nome" value="" required>
        </div>
        <div class="form-group">
            <label for="numeroAndares">Número de Andares</label>
            <input type="text" class="form-control" id="numeroAndares" name="numeroAndares" value="" required>
        </div>
        <div class="form-group">
            <label for="unidadesPAndar">Unidades por Andar</label>
            <input type="text" class="form-control" id="unidadesPAndar" name="unidadesPAndar" value="" required>
        </div>

        <input type="hidden" name="<?= \yii::$app->request->csrfParam ?>" value="<?= \yii::$app->request->csrfToken ?>">

            <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
    </form>
