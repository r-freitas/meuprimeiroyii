<?

use app\components\selectedComponent;
use app\components\maskComponent;
use yii\helpers\Url;
use app\controllers\AdministradorasController;
use app\components\estadosComponent;

?>
<form action="<?= Url::to(['condominios/realiza-edicao-condominio']); ?>" method="post" class="col-12 mt-3 formCondominio">
    <div class="container">
        <div class="row">

            
            <div class="form-group col-lg-8">
                <label for="nomeCondominio">Nome Condomínio</label>
                <input type="text" class="form-control" id="nome" name="nome" value="<?= $edit['nome'] ?>" required>
            </div>
            <div class="form-group col-md-6 col-lg-4">
                <label for="qtBlocos">Quantidade de Blocos</label>
                <input type="text" class="form-control" id="qtBlocos" name="qtBlocos" value="<?= $edit['qtBlocos'] ?>" required>
            </div>
            <div class="form-group col-md-6 col-lg-4">
                <label for="cep">CEP</label>
                <input type="text" class="form-control" id="cep" name="cep" value="<?= maskComponent::mask($edit['cep'], 'cep') ?>">
            </div>
            <div class="form-group col-md-8 col-lg-8">
                <label for="logradouro">Logradouro</label>
                <input type="text" class="form-control" id="rua" name="logradouro" value="<?= $edit['logradouro'] ?>" required>
            </div>
            <div class="form-group col-md-4 col-lg-2">
                <label for="numero">Número</label>
                <input type="text" class="form-control" id="numero" name="numero" value="<?= $edit['numero'] ?>">
            </div>
            <div class="form-group col-lg-5">
                <label for="bairro">Bairro</label>
                <input type="text" class="form-control" id="bairro" name="bairro" value="<?= $edit['bairro'] ?>">
            </div>
            <div class="form-group col-lg-5">
                <label for="cidade">Cidade</label>
                <input type="text" class="form-control" id="cidade" name="cidade" value="<?= $edit['cidade'] ?>">
            </div>
            <div class="form-group col-lg-6">
                <label for="estado">Estado</label>
                <select class="custom-select" name="estado" id="estado">
                    <option value="">Estado</option>
                    <?
                    foreach (estadosComponent::estadosSiglas() as $sig => $uf) { ?>
                        <option value="<?= $sig ?>" <?= selectedComponent::isSelected($sig, $edit['estado']) ?>><?= $uf ?></option>
                    <? } ?>
                </select>
            </div>
            <div class="form-group col-lg-6">
                <label for="id_administradora">Administradora Responsável</label>
                <select class="custom-select" name="id_administradora" id="id_administradora">
                    <option value="">Administradora</option>
                    <?php
                    foreach (AdministradorasController::listaAdministradoraSelect() as $adm) { ?>
                        <option value="<?= $adm['id'] ?>" <?= selectedComponent::isSelected($adm['id'], $edit['id_administradora']) ?>><?= $adm['nome'] ?></option>
                    <?php  } ?>
                </select>
            </div>
        
            <input type="hidden" name="<?= \yii::$app->request->csrfParam ?>" value="<?= \yii::$app->request->csrfToken ?>">
            <input type="hidden" name="id" value="<?= $edit['id'] ?>">
        </div>
        <div class="row justify-content-between">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
        </div>
    </div>
</form>