<?

use yii\helpers\Url;
use app\controllers\AdministradorasController;
use app\components\estadosComponent;

$this->title = "Cadastrar Condomínios";
?>
<h5>Cadastro de Condomínio</h5>
<form action="<?= Url::to(['condominios/realiza-cadastro-condominio']); ?>" method="post" class="col-12 mt-3 formCondominio">
    <div class="row">
        <div class="form-group col-lg-8">
            <label for="nomeCondominio">Nome Condomínio</label>
            <input type="text" class="form-control" id="nome" name="nome" value="" required>
        </div>
        <div class="form-group col-md-6 col-lg-4">
            <label for="qtBlocos">Quantidade de Blocos</label>
            <input type="text" class="form-control" id="qtBlocos" name="qtBlocos" value="" required>
        </div>
        <div class="form-group col-md-6 col-lg-4">
            <label for="cep">CEP</label>
            <input type="text" class="form-control" id="cep" name="cep" value="" required>
        </div>
        <div class="form-group col-md-8 col-lg-8">
            <label for="logradouro">Logradouro</label>
            <input type="text" class="form-control" id="rua" name="logradouro" value="" required>
        </div>
        <div class="form-group col-md-4 col-lg-2">
            <label for="numero">Número</label>
            <input type="text" class="form-control" id="numero" name="numero" value="" required>
        </div>
        <div class="form-group col-lg-5">
            <label for="bairro">Bairro</label>
            <input type="text" class="form-control" id="bairro" name="bairro" value="" required>
        </div>
        <div class="form-group col-lg-5">
            <label for="cidade">Cidade</label>
            <input type="text" class="form-control" id="cidade" name="cidade" value="" required>
        </div>
        <div class="form-group col-lg-6">
            <label for="estado">Estado</label>
            <select class="custom-select" name="estado" id="uf">
                <option value="">Estado</option>
                <?
                foreach (estadosComponent::estadosSiglas() as $sig => $uf) { ?>
                    <option value="<?= $sig ?>"><?= $uf ?></option>
                <? } ?>

            </select>
        </div>
        <div class="form-group col-lg-6">
            <label for="id_administradora">Administradora Responsável</label>
            <select class="custom-select" name="id_administradora" id="id_administradora" required>
                <option value="">Administradora</option>
                <?php
                foreach (AdministradorasController::listaAdministradoraSelect() as $adm) { ?>
                    <option value="<?= $adm['id'] ?>"><?= $adm['nome'] ?></option>
                <?php  } ?>
            </select>
            <input type="hidden" name="<?= \yii::$app->request->csrfParam ?>" value="<?= \yii::$app->request->csrfToken ?>">
        </div>
    </div>
    <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
</form>