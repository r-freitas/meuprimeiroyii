<?
use app\components\modalComponent;
use app\components\maskComponent;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\alertComponent;

$url_site = Url::base(true);
$this->title = "Listar Condomínios";
if(isset($_GET['myAlert'])){
    echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg']);
}
?>
<h1>Condomínios</h1>
<div class="row">

    <table class="table col-12 table-responsive mb-5 mt-5 tabelaCondominios">
        <thead>
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">Qt. Blocos</th>
                <th scope="col">Endereço</th>
                <th scope="col">Administradora</th>
                <th scope="col">Data Cad.</th>
                <th><a href="<?= $url_site ?>/index.php?r=condominios%2Fcadastrar-condominios"><button class="btn btn-dark">Adicionar</button></a></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach($condominios as $condo){ 
            ?>
                <tr data-id="<?= $condo['id']; ?>" class="condominio">
                    <td><?= $condo['nome'] ?></td>
                    <td><?= $condo['qtBlocos'] ?></td>
                    <td><?= $condo['logradouro'].', '.$condo['numero'].', '.$condo['bairro'].', '.$condo['cidade'].', '.$condo['estado'].', CEP '. maskComponent::mask($condo['cep'],'cep') ?></td>
                    <td><?= $condo['admNome'] ?></td>
                    <td><?= Yii::$app->formatter->format($condo['dataCadastro'], 'date') ?></td>
                    <td><a class="p-1 removerCondominio" href="<?= $url_site ?>/index.php?r=condominios/deleta-condominio&id=<?= $condo['id']; ?>"><i class="bi bi-x-circle-fill text-danger"></i></a><a class="p-1 openModal" href="<?= $url_site ?>/index.php?r=condominios/editar-condominio&id=<?= $condo['id']; ?>"><i class="bi bi-pencil-square ml-1 text-info"></i></a></td>
                </tr>
    
            <?php } ?> 
            <tr>
                <td colspan="5">&nbsp;</td>
                <td class="totalRegistros"><?php echo 'Registros: ' . ($paginacao->totalCount<10? '0'. $paginacao->totalCount : $paginacao->totalCount)?></td>
            </tr>
        </tbody>
    </table>
</div>

<?= LinkPager::widget(
    [
        'pagination' => $paginacao,
        'linkContainerOptions' => [
            'class' => 'btn-group'
        ],
        'linkOptions' => [
            'class' => 'btn btn-dark'
        ],
        'disabledListItemSubTagOptions' => [
            'class' => 'btn btn-secondary'
        ]
    ]
    ) ?>
<?= modalComponent::initModal('Edição de Condomínio') ?>