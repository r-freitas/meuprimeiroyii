<?

use app\components\selectedComponent;
use app\components\maskComponent;
use app\controllers\CondominiosController;
use app\controllers\BlocosController;
use app\controllers\UnidadesController;
use yii\helpers\Url;

?>
<form action="<?= Url::to(['moradores/realiza-edicao-morador']) ?>" method="post" class="col-12 mt-3 formMorador mb-5">
    <div class="container">
        <div class="row">
            <div class="form-group col-lg-8">
                <label for="nome">Condomínio</label>
                <select class="custom-select" name="id_condominio" id="id_condominio">
                    <option value="">Condominio</option>
                    <?php
                    foreach (CondominiosController::listaCondominioSelect() as $opcaoCond) { ?>
                        <option value="<?= $opcaoCond['id'] ?>" <?= selectedComponent::isSelected($opcaoCond['id'], $edit['id_condominio']) ?>><?= $opcaoCond['nome'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group col-lg-4">
                <label for="nome">Bloco</label>
                <select class="custom-select fromBloco" name="id_bloco" id="id_bloco">
                    <option value="">Bloco</option>
                    <?php
                    foreach (BlocosController::listaBlocoEditar($edit['id_condominio']) as $bloco) { ?>
                        <option value="<?= $bloco['id'] ?>" <?= selectedComponent::isSelected($bloco['id'], $edit['id_bloco']) ?>><?= $bloco['nome'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group col-lg-4">
                <label for="id_unidade">Unidade</label>
                <select class="custom-select fromUnidade" name="id_unidade" id="id_unidade">
                    <option value="">Unidade</option>
                    <?php
                    foreach (UnidadesController::listaUnidadeEditar($edit['id_bloco']) as $unidade) { ?>
                        <option value="<?= $unidade['id'] ?>" <?= selectedComponent::isSelected($unidade['id'], $edit['id_unidade']) ?>><?= $unidade['numeroUnidade'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group col-lg-8">
                <label for="nome">Nome</label>
                <input type="text" class="form-control" id="nome" name="nome" value="<?= $edit['nome'] ?>" required>
            </div>
            <div class="form-group col-lg-4">
                <label for="cpf">CPF</label>
                <input type="text" class="form-control" id="cpf" name="cpf" value="<?= maskComponent::mask($edit['cpf'], 'cpf') ?>" required>
            </div>
            <div class="form-group col-lg-4">
                <label for="nascimento">Nascimento</label>
                <input type="date" class="form-control" id="nascimento" name="nascimento" value="<?= $edit['nascimento'] ?>" required>
            </div>
            <div class="form-group col-lg-4">
                <label for="cpf">Telefone</label>
                <input type="text" class="form-control" id="telefone" name="telefone" value="<?= maskComponent::mask($edit['telefone'], 'telefone') ?>">
            </div>
            <div class="form-group col-12">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" value="<?= $edit['email'] ?>" required>
            </div>
            <input type="hidden" name="<?= \yii::$app->request->csrfParam ?>" value="<?= \yii::$app->request->csrfToken ?>">
            <input type="hidden" name="id" value="<?= $edit['id'] ?>">
        </div>
        <div class="row justify-content-between">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
        </div>
    </div>
</form>