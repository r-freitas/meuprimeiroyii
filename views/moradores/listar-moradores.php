<?

use app\components\maskComponent;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\alertComponent;
use app\components\modalComponent;

$url_site = Url::base(true);
$this->title = "Listar Moradores";
if(isset($_GET['myAlert'])){
    echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg']);
}

?>

<h1>Moradores</h1>
<div class="row">
    <table class="table col-12 table-responsive mb-5 mt-5 tabelaMoradores">
        <thead>
            <tr>
            </tr>
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">Condominio</th>
                <th scope="col">Bloco</th>
                <th scope="col">Unidade</th>
                <th scope="col">CPF</th>
                <th scope="col">E-mail</th>
                <th scope="col">Telefone</th>
                <th scope="col">Data Cad.</th>
                <th><a href="<?= $url_site ?>/index.php?r=moradores%2Fcadastrar-moradores"><button class="btn btn-dark">Adicionar</button></a></th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($moradores as $morador) {
            ?>
                <tr data-id="<?= $morador['id']; ?>" class="cliente">
                    <td><?= $morador['nome'] ?></td>
                    <td><?= $morador['nomeCondo'] ?></td>
                    <td><?= $morador['nomeBloco'] ?></td>
                    <td><?= $morador['numeroUnidade'] ?></td>
                    <td><?= maskComponent::mask($morador['cpf'],'cpf') ?></td>
                    <td><?= $morador['email'] ?></td>
                    <td><?= maskComponent::mask($morador['telefone'], 'telefone') ?></td>
                    <td><?= Yii::$app->formatter->format($morador['dataCadastro'], 'date') ?></td>
                    <td><a class="p-1 removerMorador" href="<?= $url_site ?>/index.php?r=moradores/deleta-morador&id=<?= $morador['id'] ?>"><i class="bi bi-x-circle-fill text-danger"></i></a><a class="p-1 openModal" href="<?= $url_site ?>/index.php?r=moradores/editar-morador&id=<?= $morador['id'] ?>"><i class="bi bi-pencil-square ml-1 text-info"></i></a></td>
                </tr>
    
            <?php } ?>
            <tr>
                <td colspan="8">&nbsp;</td>
                <td class="totalRegistros"><?php echo 'Registros: ' . ($paginacao->totalCount<10? '0'. $paginacao->totalCount : $paginacao->totalCount)?></td>
            </tr>
        </tbody>
    </table>
</div>
<?= LinkPager::widget(
    [
        'pagination' => $paginacao,
        'linkContainerOptions' => [
            'class' => 'btn-group'
        ],
        'linkOptions' => [
            'class' => 'btn btn-dark'
        ],
        'disabledListItemSubTagOptions' => [
            'class' => 'btn btn-secondary'
        ]
    ]
        );
    ?>
<?= modalComponent::initModal('Edição de Morador'); ?>