<?

use app\controllers\CondominiosController;
use app\controllers\BlocosController;
use app\controllers\UnidadesController;
use yii\helpers\Url;


$this->title = "Cadastrar Moradores";
?>
<h5>Cadastro de Morador</h5>
<form action="<?= Url::to(['moradores/realiza-cadastro-morador']) ?>" method="post" class="mt-3 formMorador mb-5">
    <div class="row">
        <div class="form-group col-md-4">
            <label for="nome">Condomínio</label>
            <select class="custom-select" name="id_condominio" id="id_condominio">
                <option value="">Condominio</option>
                <?php
                foreach (CondominiosController::listaCondominioSelect() as $opcaoCond) { ?>
                    <option value="<?= $opcaoCond['id'] ?>"><?= $opcaoCond['nome'] ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group col-md-4">
            <label for="nome">Bloco</label>
            <select class="custom-select fromBloco" name="id_bloco" id="id_bloco">

            </select>
        </div>
        <div class="form-group col-md-4">
            <label for="id_unidade">Unidade</label>
            <select class="custom-select fromUnidade" name="id_unidade" id="id_unidade">

            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" id="nome" name="nome" value="" required>
        </div>
        <div class="form-group col-md-3">
            <label for="cpf">CPF</label>
            <input type="text" class="form-control" id="cpf" name="cpf" value="" required>
        </div>
        <div class="form-group col-md-3">
            <label for="nascimento">Nascimento</label>
            <input type="date" class="form-control" id="nascimento" name="nascimento" value="" required>
        </div>
        <div class="form-group col-md-8">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" value="" required>
        </div>
        <div class="form-group col-md-4">
            <label for="cpf">Telefone</label>
            <input type="text" class="form-control" id="telefone" name="telefone" value="">
        </div>
        <input type="hidden" name="<?= \yii::$app->request->csrfParam ?>" value="<?= \yii::$app->request->csrfToken ?>">

    </div>
    <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
</form>