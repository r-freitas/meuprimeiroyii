<?
use app\components\modalComponent;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\maskComponent;
use app\components\alertComponent;

$url_site = Url::base(true);
$this->title = "Listar Administradoras";
if(isset($_GET['myAlert'])){
    echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg']);
}
?>
<h1>Administradoras</h1>
<div class="row">
    <table class="table col-12 table-responsive-sm mb-5 mt-5 tabelaBlocos">
        <thead>
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">CNPJ</th>
                <th scope="col">Data Cad.</th>
                <th><a href="<?= $url_site ?>/index.php?r=administradoras%2Fcadastrar-administradoras"><button class="btn btn-dark">Adicionar</button></a></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach($administradoras as $adm){ ?>
                <tr data-id="<?= $adm['id']; ?>" class="bloco">
                    <td><?= $adm['nome'] ?></td>
                    <td><?= maskComponent::mask($adm['cnpj'],'cnpj') ?></td>
                    <td><?= Yii::$app->formatter->format($adm['dataCadastro'], 'date') ?></td>
                    <td><a class="p-1" href="<?= $url_site ?>/index.php?r=administradoras/deleta-administradora&id=<?php echo $adm['id']; ?>"><i class="bi bi-x-circle-fill text-danger"></i></a><a class="p-1 openModal" href="<?= $url_site ?>/index.php?r=administradoras/editar-administradora&id=<?php echo $adm['id']; ?>"><i class="bi bi-pencil-square ml-1 text-info"></i></a></td>
                </tr>
            <?php } ?> 
            <tr>
                <td colspan="3">&nbsp;</td>
                <td class="totalRegistros"><?php echo 'Registros: '.$paginacao->totalCount ?></td>
            </tr>
        </tbody>
    </table>
</div>

<?= LinkPager::widget(
    [
        'pagination' => $paginacao,
        'linkContainerOptions' => [
            'class' => 'btn-group'
        ],
        'linkOptions' => [
            'class' => 'btn btn-dark'
        ],
        'disabledListItemSubTagOptions' => [
            'class' => 'btn btn-secondary'
        ]
    ]
    ) ?>

<?= modalComponent::initModal('Edição de Administradora') ?>