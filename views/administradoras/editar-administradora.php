<?
use app\components\maskComponent;
use yii\helpers\Url;

?>
<form action="<?= Url::to(['administradoras/realiza-edicao-administradora']) ?>" method="post" class="col-12 mt-3 formAdministradora">
    <div class="form-group">
        <label for="nome">Nome</label>
        <input type="text" class="form-control" id="nome" name="nome" value="<?= $edit['nome'] ?>" required>
    </div>
    <div class="form-group">
        <label for="cnpj">CNPJ</label>
        <input type="text" class="form-control" id="cnpj" name="cnpj" value="<?= maskComponent::mask($edit['cnpj'], 'cnpj') ?>" required>
    </div>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam ?>" value="<?= \yii::$app->request->csrfToken ?>">
    <input type="hidden" name="id" value="<?= $edit['id'] ?>">
    <div class="row justify-content-between">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
    </div>
</form>