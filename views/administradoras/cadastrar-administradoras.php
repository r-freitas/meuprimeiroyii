<?
use yii\helpers\Html;
use yii\widgets\linkPager;
use yii\helpers\Url;
$this->title = "Cadastrar Administradoras";
?>
    <h5>Cadastro de Administradora</h5>
    <form action="<?= Url::to(['administradoras/realiza-cadastro-administradora']) ?>" method="post" class="col-12 mt-3 formAdministradora">
        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" id="nome" name="nome" value="" required>
        </div>
        <div class="form-group">
            <label for="cnpj">CNPJ</label>
            <input type="text" class="form-control" id="cnpj" name="cnpj" value="" required>
        </div>

            <input type="hidden" name="<?= \yii::$app->request->csrfParam ?>" value="<?= \yii::$app->request->csrfToken ?>">

            <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
    </form>