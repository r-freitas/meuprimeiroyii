<?

use yii\helpers\Url;
$this->title = "Cadastrar Usuários";

?>
    <h5>Cadastro de Usuários</h5>
    <form action="<?= Url::to(['usuarios/realiza-cadastro-usuario']) ?>" method="post" class="col-12 mt-3 formUsuario">
        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" id="nome" name="nome" value="" required>
        </div>
        <div class="form-group">
            <label for="usuario">Usuário</label>
            <input type="text" class="form-control" id="usuario" name="usuario" value="" required>
        </div>
        <div class="form-group">
            <label for="usuario">Senha</label>
            <input type="password" class="form-control" id="senha" name="senha" value="" required>
        </div>

            <input type="hidden" name="<?= \yii::$app->request->csrfParam ?>" value="<?= \yii::$app->request->csrfToken ?>">

            <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
    </form>