<?

use yii\helpers\Url;

?>
<form action="<?= Url::to(['usuarios/realiza-edicao-usuario']) ?>" method="post" class="col-12 mt-3 formUsuario">
    <div class="form-group">
        <label for="nome">Nome</label>
        <input type="text" class="form-control" id="nome" name="nome" value="<?= $edit['nome'] ?>" required>
    </div>
    <div class="form-group">
        <label for="usuario">Usuário</label>
        <input type="text" class="form-control" id="usuario" name="usuario" value="<?= $edit['usuario'] ?>" required>
    </div>
    <div class="form-group">
        <label for="senha">Senha</label>
        <input type="password" class="form-control" id="senha" name="senha" value="<?= $edit['senha'] ?>" required>
    </div>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam ?>" value="<?= \yii::$app->request->csrfToken ?>">
    <input type="hidden" name="id" value="<?= $edit['id'] ?>">
    <div class="row justify-content-between">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
    </div>
</form>