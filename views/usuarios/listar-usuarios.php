<?
use app\components\modalComponent;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\alertComponent;

$url_site = Url::base(true);
$this->title = "Listar Usuários";
if(isset($_GET['myAlert'])){
    echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg']);
}
?>
<h1>Usuários</h1>
<div class="row">
    <table class="table col-12 table-responsive-lg mb-5 mt-5 tabelaBlocos">
        <thead>
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">Usuário</th>
                <th scope="col">Data Cad.</th>
                <th><a href="<?= $url_site ?>/index.php?r=usuarios%2Fcadastrar-usuarios"><button class="btn btn-dark">Adicionar</button></a></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach($usuarios as $user){ ?>
                <tr data-id="<?= $user['id']; ?>" class="bloco">
                    <td><?= $user['nome'] ?></td>
                    <td><?= $user['usuario'] ?></td>
                    <td><?= Yii::$app->formatter->format($user['dataCadastro'], 'date') ?></td>
                    <td><a class="p-1" href="<?= $url_site ?>/index.php?r=usuarios/deleta-usuario&id=<?php echo $user['id']; ?>"><i class="bi bi-x-circle-fill text-danger"></i></a><a class="p-1 openModal" href="<?= $url_site ?>/index.php?r=usuarios/editar-usuario&id=<?php echo $user['id']; ?>"><i class="bi bi-pencil-square ml-1 text-info"></i></a></td>
                </tr>
            <?php } ?> 
            <tr>
                <td colspan="3">&nbsp;</td>
                <td class="totalRegistros"><?php echo 'Registros: '.$paginacao->totalCount ?></td>
            </tr>
        </tbody>
    </table>
</div>

<?= LinkPager::widget(
    [
        'pagination' => $paginacao,
        'linkContainerOptions' => [
            'class' => 'btn-group'
        ],
        'linkOptions' => [
            'class' => 'btn btn-dark'
        ],
        'disabledListItemSubTagOptions' => [
            'class' => 'btn btn-secondary'
        ]
    ]
    ) ?>

<?= modalComponent::initModal('Edição de Usuário') ?>