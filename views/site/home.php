<?
$this->title = "Dashboard";
?>
<div class="row justify-content-center">
    <div class="col-lg-4 p-3">
        <div class="row justify-content-center mb-5">
            <h2>Total Cadastros</h2>
            <small class="col-12 text-center mb-3">Indica o tamanho do sistema</small>
            <div class="card col-10">
                <div class="card-header bg-white font-weight-bold">
                    Usuários do Sistema:
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Administradoras: <span class="badge badge-warning float-right"><?= ($administradoras > 9 ? $administradoras : '0' . $administradoras) ?></span></li>
                    <li class="list-group-item">Condomínios: <span class="badge badge-warning float-right"><?= ($condominios > 9 ? $condominios : '0' . $condominios) ?></span></li>
                    <li class="list-group-item">Blocos: <span class="badge badge-warning float-right"><?= ($blocos > 9 ? $blocos : '0' . $blocos) ?></span></li>
                    <li class="list-group-item">Unidades: <span class="badge badge-warning float-right"><?= ($unidades > 9 ? $unidades : '0' . $unidades) ?></span></li>
                    <li class="list-group-item">Moradores: <span class="badge badge-warning float-right"><?= ($moradores > 9 ? $moradores : '0' . $moradores) ?></span></li>
                    <li class="list-group-item">Usuários: <span class="badge badge-warning float-right"><?= ($usuarios > 9 ? $usuarios : '0' . $usuarios) ?></span></li>
                    <li class="list-group-item">Conselheiros: <span class="badge badge-warning float-right"><?= ($conselheiros > 9 ? $conselheiros : '0' . $conselheiros) ?></span></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-lg-8 p-3">
        <div class="row justify-content-center">
            <h2>Cadastros Nos Últimos 5 Dias</h2>
            <small class="col-12 text-center mb-3">Indica o ritmo de crescimento do sistema para os principais tipos de clientes</small>
            <canvas id="myChart"></canvas>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
<script>
    var xValues = ['<?= $ultimosDias[4]. "', '". $ultimosDias[3] ."', '".  $ultimosDias[2] ."', '".  $ultimosDias[1] ."', '". $ultimosDias[0] ?>'];

    new Chart("myChart", {
        type: "line",
        data: {
            labels: xValues,
            datasets: [
                {
                label: 'Moradores',
                data: [<?= $moradoresGrafico['dMenosQuatro'].', '.$moradoresGrafico['dMenosTres'].', '.$moradoresGrafico['dMenosDois'].', '.$moradoresGrafico['dMenosUm'].', '.$moradoresGrafico['hoje'] ?>],
                borderColor: "yellow",
                fill: false
            }, {
                label: 'Condomínios',
                data: [<?= $condominiosGrafico['dMenosQuatro'].', '.$condominiosGrafico['dMenosTres'].', '.$condominiosGrafico['dMenosDois'].', '.$condominiosGrafico['dMenosUm'].', '.$condominiosGrafico['hoje'] ?>],
                borderColor: "black",
                fill: false
            }, {
                label: 'Administradoras',
                data: [<?= $administradorasGrafico['dMenosQuatro'].', '.$administradorasGrafico['dMenosTres'].', '.$administradorasGrafico['dMenosDois'].', '.$administradorasGrafico['dMenosUm'].', '.$administradorasGrafico['hoje'] ?>],
                borderColor: "orange",
                fill: false
            }]
        },
        options: {
            legend: {
                display: true
            }
        }
    });
</script>