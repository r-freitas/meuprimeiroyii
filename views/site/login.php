<?php

use app\components\alertComponent;
use yii\helpers\Url;
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <link rel="shortcut icon" href="#">
    <title>Login</title>
</head>

<body>
    <main class="container mt-3 mb-5">
        <?= (isset($_GET['myAlert']) ? alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg'],$_GET['myAlert']['redir']) : ''); ?>
        <div class="row justify-content-center">

            <form action="<?= Url::to(['site/login']) ?>" method="post" class="col-sm-4 bg-dark rounded shadow p-4">
                <center><img class="mb-4 text-center" src="img/logo.png" alt="" width="142"></center>
                <h1 class="h3 mb-3 fw-normal text-white">Login</h1>

                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingInput" placeholder="usuario" name="usuario" required>
                    <label for="floatingInput" class="text-white">Usuário</label>
                </div>
                <div class="form-floating">
                    <input type="password" class="form-control" id="floatingPassword" placeholder="senha" name="senha" required>
                    <label for="floatingPassword" class="text-white">Senha</label>
                </div>

                <div class="checkbox mb-3">
                    <label class="text-white">
                        <input type="checkbox" value="remember-me"> Manter logado
                    </label>
                </div>
                <input type="hidden" name="<?= \yii::$app->request->csrfParam ?>" value="<?= \yii::$app->request->csrfToken ?>">
                <button class="w-100 btn btn-lg btn-warning" type="submit">Entrar</button>
            </form>
        </div>
    </main>
<script src="js/jquery-3.6.0.min.js"></script>
</body>

</html>