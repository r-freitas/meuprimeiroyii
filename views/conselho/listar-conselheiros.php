<?

use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\modalComponent;
use app\components\alertComponent;
use app\components\maskComponent;

$url_site = Url::base(true);
$this->title = "Listar Conselheiros";
if(isset($_GET['myAlert'])){
    echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg']);
}
?>
<h1>Conselheiros</h1>
<div class="row">
    <table class="table col-12 table-responsive-lg mb-5 mt-5 tabelaConselho">
        <thead>
            <tr>
                <th scope="col">Condomínio</th>
                <th scope="col">Função</th>
                <th scope="col">Nome</th>
                <th scope="col">CPF</th>
                <th scope="col">E-mail</th>
                <th scope="col">Telefone</th>
                <th scope="col">Data Cad.</th>
                <th><a href="<?= $url_site ?>/index.php?r=conselho%2Fcadastrar-conselheiros"><button class="btn btn-dark">Adicionar</button></a></th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($conselheiros as $valor) {
            ?>
                <tr data-id="<?= $valor['id'] ?>" class="conselho">
                    <td><?=$valor['nomeCondo'] ?></td>
                    <td><?= $valor['funcao'] ?></td>
                    <td><?= $valor['nomeConselho'] ?></td>
                    <td><?= maskComponent::mask($valor['cpf'], 'cpf') ?></td>
                    <td><?= $valor['email'] ?></td>
                    <td><?= maskComponent::mask($valor['telefone'], 'telefone') ?></td>
                    <td><?= $valor['dataCadastro'] ?></td>
                    <td><a class="p-1 removerConselho" href="<?= $url_site ?>/index.php?r=conselho/deleta-conselheiro&id=<?= $valor['id'] ?>"><i class="bi bi-x-circle-fill text-danger"></i></a><a class="p-1 openModal" href="<?= $url_site ?>/index.php?r=conselho/editar-conselheiro&id=<?php echo $valor['id']; ?>"><i class="bi bi-pencil-square ml-1 text-info"></i></a></td>
                </tr>
    
            <?php } ?>
            <tr>
                <td colspan="7">&nbsp;</td>
                <td class="totalRegistros"><?php echo 'Registros: ' . ($paginacao->totalCount<10? '0'. $paginacao->totalCount : $paginacao->totalCount)?></td>
            </tr>
        </tbody>
    </table>
</div>

<?= LinkPager::widget(
    [
        'pagination' => $paginacao,
        'linkContainerOptions' => [
            'class' => 'btn-group'
        ],
        'linkOptions' => [
            'class' => 'btn btn-dark'
        ],
        'disabledListItemSubTagOptions' => [
            'class' => 'btn btn-secondary'
        ]
    ]
) ?>
    <?= modalComponent::initModal('Edição de Conselheiro'); ?>