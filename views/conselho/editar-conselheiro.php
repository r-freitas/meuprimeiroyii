<?

use app\components\selectedComponent;
use yii\helpers\Html;
use app\controllers\CondominiosController;
use app\components\maskComponent;
use yii\helpers\Url;

$url_site = Url::base(true);
?>

<form action="<?= Url::to(['conselho/realiza-edicao-conselheiro']) ?>" method="post" class="col-12 mt-3 formConselho mb-5">
    <div class="form-group">
        <label for="condominio">Selecione o Condomínio</label>
        <select class="custom-select" name="id_condominio" id="id_condominio">
            <option value="">Condominio</option>
            <?php
            foreach (CondominiosController::listaCondominioSelect() as $opcaoCond) { ?>
                <option value="<?= $opcaoCond['id'] ?>" <?= selectedComponent::isSelected($opcaoCond['id'], $edit['id_condominio']) ?>><?= $opcaoCond['nome'] ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group">
        <label for="funcao">Função</label>
        <select id="funcao" name="funcao" class="custom-select">
            <option value="Síndico">Síndico</option>
            <option value="Subsíndico">Subsíndico</option>
            <option value="Conselheiro">Conselheiro</option>
        </select>
    </div>
    <div class="form-group">
        <label for="nomeConselho">Nome</label>
        <input type="text" class="form-control" id="nomeConselho" name="nomeConselho" value="<?= $edit['nomeConselho'] ?>" required>
    </div>
    <div class="form-group">
        <label for="cpf">CPF</label>
        <input type="text" class="form-control" id="cpf" name="cpf" value="<?= maskComponent::mask($edit['cpf'], 'cpf') ?>" required>
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" value="<?= $edit['email'] ?>" required>
    </div>
    <div class="form-group">
        <label for="cpf">Telefone</label>
        <input type="text" class="form-control" id="telefone" name="telefone" value="<?= maskComponent::mask($edit['telefone'], 'telefone') ?>">
    </div>
    <input type="hidden" name="<?= \yii::$app->request->csrfParam ?>" value="<?= \yii::$app->request->csrfToken ?>">
    <input type="hidden" name="id" value="<?= $edit['id'] ?>">
    <div class="row justify-content-between">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
    </div>
</form>