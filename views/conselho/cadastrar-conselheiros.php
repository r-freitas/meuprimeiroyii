<?

use yii\helpers\Html;
use app\controllers\CondominiosController;
use app\controllers\ConselhoController;
use yii\helpers\Url;
$this->title = "Cadastrar Conselheiros";
?>
<h5>Cadastro de Conselho</h5>
    <form action="<?= Url::to(['conselho/realiza-cadastro-conselheiro']) ?>" method="post" class="mt-3 row formConselho mb-5">
        <div class="form-group col-md-6">
            <label for="condominio">Selecione o Condomínio</label>
            <select class="custom-select" name="id_condominio" id="id_condominio">
                <option value="">Condominio</option>
                <?php
                foreach (CondominiosController::listaCondominioSelect() as $opcaoCond) { ?>
                    <option value="<?= $opcaoCond['id'] ?>"><?= $opcaoCond['nome'] ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="funcao">Função</label>
            <select id="funcao" name="funcao" class="custom-select">
                <option value="Síndico">Síndico</option>
                <option value="Subsíndico">Subsíndico</option>
                <option value="Conselheiro">Conselheiro</option>
            </select>
        </div>
        <div class="form-group col-md-8">
            <label for="nomeConselho">Nome</label>
            <input type="text" class="form-control" id="nomeConselho" name="nomeConselho" value="" required>
        </div>
        <div class="form-group col-md-4">
            <label for="cpf">CPF</label>
            <input type="text" class="form-control" id="cpf" name="cpf" value="" required>
        </div>
        <div class="form-group col-md-8">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" value="" required>
        </div>
        <div class="form-group col-md-4">
            <label for="cpf">Telefone</label>
            <input type="text" class="form-control" id="telefone" name="telefone" value="">
        </div>
        <input type="hidden" name="<?= \yii::$app->request->csrfParam ?>" value="<?= \yii::$app->request->csrfToken ?>">
        <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>

</form>