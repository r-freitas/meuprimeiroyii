<?
namespace app\modules\api\controllers;
use app\models\AdministradorasModel;
use app\models\CondominiosModel;
use Exception;
use yii\web\Controller;

class CondominiosController extends Controller{

    public function behaviors() {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost', 'https://localhost'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
    
            ],
        ];
    }

    public function actionGetAll(){
        $condTable = CondominiosModel::tableName();
        $admTable = AdministradorasModel::tableName();
        $qry = (new \yii\db\Query())
            ->select(
                'cond.id,
                cond.id_administradora, 
                adm.nome AS admNome,
                cond.nome,
                cond.qtBlocos,
                cond.cep,
                cond.logradouro,
                cond.numero,
                cond.bairro,
                cond.cidade,
                cond.estado,
                cond.dataCadastro,
                cond.dataModificado'
            )
            ->from($condTable.' cond')
            ->innerJoin($admTable.' adm', 'adm.id = cond.id_administradora');
        $data = $qry->orderBy('nome')->all();
        $dados = [];
        $i=0;

        if($qry->count()>0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();

            foreach($data as $d){
                foreach($d as $ch=>$r){
                    $dados['resultSet'][$i][$ch] = $r;
                }
                $i++;
            }
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo';
        }
        return json_encode($dados);
    }

    public function actionGetOne(){
        $request = \yii::$app->request;
        $qry = CondominiosModel::find();
        $d = $qry->where(['id' => $request->get('id')])->one();

        if($qry->count()>0){
            $dados['endPoint']['status'] = 'success';
            $dados['endPoint']['status'] = 'success';
            $dados['resultSet'][0]['id'] = $d['id'];
            $dados['resultSet'][0]['nome'] = $d['nome'];
            $dados['resultSet'][0]['id_administradora'] = $d['id_administradora'];
            $dados['resultSet'][0]['qtBlocos'] = $d['qtBlocos'];
            $dados['resultSet'][0]['cep'] = $d['cep'];
            $dados['resultSet'][0]['logradouro'] = $d['logradouro'];
            $dados['resultSet'][0]['numero'] = $d['numero'];
            $dados['resultSet'][0]['bairro'] = $d['bairro'];
            $dados['resultSet'][0]['cidade'] = $d['cidade'];
            $dados['resultSet'][0]['estado'] = $d['estado'];
            $dados['resultSet'][0]['dataCadastro'] = $d['dataCadastro'];
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }
        return json_encode($dados);
    }

    public function actionRegisterCondominio(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = new CondominiosModel();
                $model->attributes = $request->post();
                $model->save();

                $dados=[];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Seu registro foi inserido com sucesso.';

                return json_encode($dados);
            }
        }catch (Exception $e){
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Seu registro não foi inserido.';

            return json_encode($dados);
        }
    }

    public function actionEditCondominio(){
        $request = \yii::$app->request;
        try{
            if($request->isPost){
                $model = CondominiosModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Seu registro foi editado com sucesso';

                return json_encode($dados);
            }
        }catch (Exception $e){
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não foi possível editar o registro.';

            return json_encode($dados);
        }
    }

    public function actionDeleteCondominio()
    {
        $request = \yii::$app->request;
        try {
            if ($request->isPost) {
                $model = CondominiosModel::findOne($request->post('id'));
                $model->delete();
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Seu registro foi deletado com sucesso.';

                return json_encode($dados);
            }
        } catch (Exception $e) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não foi possível deletar o registro.';
            $dados['endPoint']['error'] = $e;

            return json_encode($dados);
        }
    }    

}

?>