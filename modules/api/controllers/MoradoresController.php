<?

namespace app\modules\api\controllers;

use app\models\MoradoresModel;
use Exception;
use yii\web\Controller;

class MoradoresController extends Controller{

    public function behaviors() {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost', 'https://localhost'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
    
            ],
        ];
    }

    public function actionGetAll(){
        $qry = MoradoresModel::find();
        $data = $qry->orderBy('id_condominio')->all();
        $dados=[];
        $i=0;

        if($qry->count()>0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();

            foreach($data as $d){
                $dados['resultSet'][$i]['id'] = $d['id'];
                $dados['resultSet'][$i]['id_condominio'] = $d['id_condominio'];
                $dados['resultSet'][$i]['id_bloco'] = $d['id_bloco'];
                $dados['resultSet'][$i]['id_unidade'] = $d['id_unidade'];
                $dados['resultSet'][$i]['nome'] = $d['nome'];
                $dados['resultSet'][$i]['cpf'] = $d['cpf'];
                $dados['resultSet'][$i]['nascimento'] = $d['nascimento'];
                $dados['resultSet'][$i]['email'] = $d['email'];
                $dados['resultSet'][$i]['telefone'] = $d['telefone'];
                $dados['resultSet'][$i]['dataCadastro'] = $d['dataCadastro'];
                $i++;
            }
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo';
        }
        return json_encode($dados);
    }

    public function actionGetOne(){
        $request = \yii::$app->request;
        $qry = MoradoresModel::find();
        $d = $qry->where(['id' => $request->get('id')])->one();

        if($qry->count()>0){
            $dados['resultSet'][0]['id'] = $d['id'];
            $dados['resultSet'][0]['id_condominio'] = $d['id_condominio'];
            $dados['resultSet'][0]['id_bloco'] = $d['id_bloco'];
            $dados['resultSet'][0]['id_unidade'] = $d['id_unidade'];
            $dados['resultSet'][0]['nome'] = $d['nome'];
            $dados['resultSet'][0]['cpf'] = $d['cpf'];
            $dados['resultSet'][0]['nascimento'] = $d['nascimento'];
            $dados['resultSet'][0]['email'] = $d['email'];
            $dados['resultSet'][0]['telefone'] = $d['telefone'];
            $dados['resultSet'][0]['dataCadastro'] = $d['dataCadastro'];
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo';
        }
        return json_encode($dados);
    }

    public function actionRegisterMorador(){
        $request = \yii::$app->request;

        try{
            if($request->isPost){
                $model = new MoradoresModel();
                $model->attributes = $request->post();
                $model->save();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Seu registro foi inserido com sucesso.';
            }
        }catch(Exception $e){
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';

            return json_encode($dados);
        }
    }

    public function actionEditMorador(){
        $request = \yii::$app->request;
        try{
            if($request->isPost){
                $model = MoradoresModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Seu registro foi editado com sucesso.';

                return json_encode($dados);
            }
        }catch(Exception $e){
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não foi possível editar o registro.';
            
            return json_encode($dados);
        }
    }


    public function actionDeleteMorador(){
        $request = \yii::$app->request;
        try{
            if($request->isPost){
                $model = MoradoresModel::findOne($request->post('id'));
                $model->delete();
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Seu registro foi deletado com sucesso.';

                return json_encode($dados);
            }
        }catch(Exception $e){
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não foi possível deletar o registro.';
            $dados['endPoint']['error'] = $e;
            
            return json_encode($dados);
        }
    }


}

?>