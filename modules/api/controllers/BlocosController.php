<?

namespace app\modules\api\controllers;

use app\models\BlocosModel;
use app\models\CondominiosModel;
use Exception;
use yii\web\Controller;

class BlocosController extends Controller
{

    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost', 'https://localhost'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],

            ],
        ];
    }

    public function actionGetAll(){
        $blocoTable = BlocosModel::tableName();
        $condTable = CondominiosModel::tableName();
        $qry = (new \yii\db\Query())
            ->select(
                'bloco.id,
                bloco.id_condominio, 
                condo.nome AS admNome,
                bloco.nome,
                bloco.numeroAndares,
                bloco.unidadesPAndar,
                condo.logradouro,
                condo.numero,
                condo.bairro,
                condo.cidade,
                condo.estado,
                condo.dataCadastro,
                condo.dataModificado'
            )
            ->from($blocoTable.' bloco')
            ->innerJoin($condTable.' condo', 'bloco.id_condominio = condo.id');
        $data = $qry->orderBy('nome')->all();
        $dados = [];
        $i=0;

        if($qry->count()>0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();

            foreach($data as $d){
                foreach($d as $ch=>$r){
                    $dados['resultSet'][$i][$ch] = $r;
                }
                $i++;
            }
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo';
        }
        return json_encode($dados);
    }

    public function actionGetOne()
    {
        $request = \yii::$app->request;
        $qry = BlocosModel::find();
        $d = $qry->where(['id' => $request->get('id')])->one();

        if ($qry->count() > 0) {
            $dados['endPoint']['status'] = 'success';
            $dados['endPoint'][0]['id'] = $d['id'];
            $dados['endPoint'][0]['id_condominio'] = $d['id_condominio'];
            $dados['endPoint'][0]['nome'] = $d['nome'];
            $dados['endPoint'][0]['numeroAndares'] = $d['numeroAndares'];
            $dados['endPoint'][0]['unidadesPAndar'] = $d['unidadesPAndar'];
            $dados['endPoint'][0]['dataCadastro'] = $d['dataCadastro'];
        } else {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo';
        }
        return json_encode($dados);
    }

    public function actionRegisterBloco()
    {
        $request = \yii::$app->request;

        try {
            if ($request->isPost) {
                $model = new BlocosModel();
                $model->attributes = $request->post();
                $model->save();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Seu registro foi inserido com sucesso.';
            }
        } catch (Exception $e) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';

            return json_encode($dados);
        }
    }

    public function actionEditBloco()
    {
        $request = \yii::$app->request;
        try {
            if ($request->isPost) {
                $model = BlocosModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Seu registro foi editado com sucesso.';

                return json_encode($dados);
            }
        } catch (Exception $e) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não foi possível editar o registro.';

            return json_encode($dados);
        }
    }


    public function actionDeleteBloco()
    {
        $request = \yii::$app->request;
        try {
            if ($request->isPost) {
                $model = BlocosModel::findOne($request->post('id'));
                $model->delete();
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Seu registro foi deletado com sucesso.';

                return json_encode($dados);
            }
        } catch (Exception $e) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não foi possível deletar o registro.';
            $dados['endPoint']['error'] = $e;

            return json_encode($dados);
        }
    }

    public function actionGetBlocosCond(){
        $request = \yii::$app->request;
        $qry = BlocosModel::find();
        $data = $qry->where(['id_condominio' => $request->get('id_condominio')])->orderBy('nome')->all();
        $dados = [];
        $i = 0;
        if ($qry->count() > 0) {
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();

            foreach ($data as $d) {
                $dados['resultSet'][$i]['id'] = $d['id'];
                $dados['resultSet'][$i]['id_condominio'] = $d['id_condominio'];
                $dados['resultSet'][$i]['nome'] = $d['nome'];
                $i++;
            }
        } else {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo';
        }
        return json_encode($dados);
    }
}
