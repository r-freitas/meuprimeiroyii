<h1>Orientações de uso da API</h1>
<div class="accordion" id="accordionExample">

  <div class="card">
    <div class="card-header" id="headingZero">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseZero" aria-expanded="true" aria-controls="collapseZero">
          OBTER TOKEN PARA POSTS (OBRIGATÓRIO)
        </button>
      </h2>
    </div>

    <div id="collapseZero" class="collapse" aria-labelledby="headingZero" data-parent="#accordionExample">
      <div class="card-body">
        <p><code>METHOD: </code><small>GET</small></p>
        <p><code>URL: </code><small>http://localhost/meuprimeiroyii/web/index.php?r=api/default/get-token-post</small></p>
        <p><code>PARAMS: </code><small>null</small></p>
      </div>
    </div>
  </div>

  <!--  -->
    <div class="card">
      <div class="card-header" id="headingdois">
        <h2 class="mb-0">
          <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapsedois" aria-expanded="true" aria-controls="collapsedois">
            LISTAR TODOS
          </button>
        </h2>
      </div>

      <div id="collapsedois" class="collapse" aria-labelledby="headingdois" data-parent="#accordionExample">
        <div class="card-body">
          <p><code>METHOD: </code><small>GET</small></p>
          <p><code>URL: </code><small>http://localhost/meuprimeiroyii/web/index.php?r=api/<strong>&lt;objeto&gt;</strong>/<strong>&lt;ação&gt;</strong></small></p>
          <p><strong>objeto:</strong> administradoras / <strong>ação:</strong> get-all -> PARAMS: <small>null</small></p>
          <p><strong>objeto:</strong> condominios /<strong> ação:</strong> get-all -> PARAMS: <small>null</small></p>
          <p><strong>objeto:</strong> conselho /<strong> ação:</strong> get-all -> PARAMS: <small>null</small></p>
          <p><strong>objeto:</strong> blocos /<strong> ação:</strong> get-all -> PARAMS: <small>null</small></p>
          <p><strong>objeto:</strong> moradores /<strong> ação:</strong> get-all -> PARAMS: <small>null</small></p>
          <p><strong>objeto:</strong> unidades /<strong> ação:</strong> get-all -> PARAMS: <small>null</small></p>
        </div>
      </div>
    </div>

    <!--  -->

    <div class="card">
      <div class="card-header" id="headingtres">
        <h2 class="mb-0">
          <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapsetres" aria-expanded="false" aria-controls="collapsetres">
            LISTAR APENAS UM(A)
          </button>
        </h2>
      </div>

      <div id="collapsetres" class="collapse" aria-labelledby="headingtres" data-parent="#accordionExample">
        <div class="card-body">
          <p><code>METHOD: </code><small>GET</small></p>
          <p><code>URL: </code><small>http://localhost/meuprimeiroyii/web/index.php?r=api/<strong>&lt;objeto&gt;</strong>/<strong>&lt;ação&gt;</strong></small></p>
          <p><strong>objeto:</strong> administradoras / <strong>ação:</strong> get-one -> PARAMS: <small>id (int)</small></p>
          <p><strong>objeto:</strong> condominios / <strong>ação:</strong> get-one -> PARAMS: <small>id (int)</small></p>
          <p><strong>objeto:</strong> conselho / <strong>ação:</strong> get-one -> PARAMS: <small>id (int)</small></p>
          <p><strong>objeto:</strong> blocos / <strong>ação:</strong> get-one -> PARAMS: <small>id (int)</small></p>
          <p><strong>objeto:</strong> moradores / <strong>ação:</strong> get-one -> PARAMS: <small>id (int)</small></p>
          <p><strong>objeto:</strong> unidades / <strong>ação:</strong> get-one -> PARAMS: <small>id (int)</small></p>
        </div>
      </div>
    </div>
    
    <!--  -->

    <div class="card">
      <div class="card-header" id="headingquatro">
        <h2 class="mb-0">
          <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapsequatro" aria-expanded="false" aria-controls="collapsequatro">
            CADASTRAR
          </button>
        </h2>
      </div>

      <div id="collapsequatro" class="collapse" aria-labelledby="headingquatro" data-parent="#accordionExample">
        <div class="card-body">
          <p><code>METHOD: </code><small>POST</small></p>
          <p><code>URL: </code><small>http://localhost/meuprimeiroyii/web/index.php?r=api/<strong>&lt;objeto&gt;</strong>/<strong>&lt;ação&gt;</strong></small></p>
          <p><strong>objeto:</strong> administradoras /<strong> ação:</strong> register-adm -> PARAMS: <small>_csrf (token), nome (varchar), cnpj (varchar)</small></p>
          <p><strong>objeto:</strong> condominios /<strong> ação:</strong> register-condominio -> PARAMS: <small>_csrf (token), nome (varchar), qtBlocos (int), cep (varchar), logradouro (varchar), numero (varchar), bairro (varchar), cidade (varchar), estado (varchar), id_administradora (int)</small></p>
          <p><strong>objeto:</strong> conselho /<strong> ação:</strong> register-conselheiro -> PARAMS: <small>_csrf (token), nomeConselho (varchar), funcao (varchar), id_condominio (int), cpf (varchar), email (varchar), telefone (varchar)</small></p>
          <p><strong>objeto:</strong> blocos /<strong> ação:</strong> register-bloco -> PARAMS: <small>_csrf (token), id_condoominio (int), nome (varchar), numeroAndares (int), unidadesPAndar (int)</small></p>
          <p><strong>objeto:</strong> moradores /<strong> ação:</strong> register-morador -> PARAMS: <small>_csrf (token), nome (varchar), cpf (varchar), id_condominio (int), id_bloco (int), id_unidade (int), email (varchar), telefone (varchar)</small></p>
          <p><strong>objeto:</strong> unidades /<strong> ação:</strong> register-unidade -> PARAMS: <small>_csrf (token), numero (varchar), metragem (double), id_condominio (int), id_bloco (int), vagasDeGaragem (int)</small></p>
        </div>
      </div>
    </div>

    <!--  -->

    <div class="card">
      <div class="card-header" id="headingcinco">
        <h2 class="mb-0">
          <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapsecinco" aria-expanded="false" aria-controls="collapsecinco">
            EDITAR
          </button>
        </h2>
      </div>

      <div id="collapsecinco" class="collapse" aria-labelledby="headingcinco" data-parent="#accordionExample">
        <div class="card-body">
          <p><code>METHOD: </code><small>POST</small></p>
          <p><code>URL: </code><small>http://localhost/meuprimeiroyii/web/index.php?r=api/<strong>&lt;objeto&gt;</strong>/<strong>&lt;ação&gt;</strong></small></p>
          <p><strong>objeto:</strong> administradoras /<strong> ação:</strong> edit-adm -> PARAMS: <small>id (int), _csrf (token), nome (varchar), cnpj (varchar)</small></p>
          <p><strong>objeto:</strong> condominios /<strong> ação:</strong> edit-condominio -> PARAMS: <small>id (int), _csrf (token), nome (varchar), qtBlocos (int), cep (varchar), logradouro (varchar), numero (varchar), bairro (varchar), cidade (varchar), estado (varchar), id_administradora (int)</small></p>
          <p><strong>objeto:</strong> conselho /<strong> ação:</strong> edit-conselheiro -> PARAMS: <small>id (int), _csrf (token), nomeConselho (varchar), funcao (varchar), id_condominio (int), cpf (varchar), email (varchar), telefone (varchar)</small></p>
          <p><strong>objeto:</strong> blocos /<strong> ação:</strong> edit-bloco -> PARAMS: <small>id (int), _csrf (token), id_condoominio (int), nome (varchar), numeroAndares (int), unidadesPAndar (int)</small></p>
          <p><strong>objeto:</strong> moradores /<strong> ação:</strong> edit-morador -> PARAMS: <small>id (int), _csrf (token), nome (varchar), cpf (varchar), id_condominio (int), id_bloco (int), id_unidade (int), email (varchar), telefone (varchar)</small></p>
          <p><strong>objeto:</strong> unidades /<strong> ação:</strong> edit-unidade -> PARAMS: <small>id (int), _csrf (token), numero (varchar), metragem (double), id_condominio (int), id_bloco (int), vagasDeGaragem (int)</small></p>
        </div>
      </div>
    </div>

    <!--  -->

    <div class="card">
      <div class="card-header" id="headingseis">
        <h2 class="mb-0">
          <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseseis" aria-expanded="false" aria-controls="collapseseis">
            DELETAR
          </button>
        </h2>
      </div>

      <div id="collapseseis" class="collapse" aria-labelledby="headingseis" data-parent="#accordionExample">
        <div class="card-body">
          <p><code>METHOD: </code><small>POST</small></p>
          <p><code>URL: </code><small>http://localhost/meuprimeiroyii/web/index.php?r=api/<strong>&lt;objeto&gt;</strong>/<strong>&lt;ação&gt;</strong></small></p>
          <p><strong>objeto:</strong> administradoras /<strong> ação:</strong> delete-adm -> PARAMS: <small>id (int), _csrf (token)</small></p>
          <p><strong>objeto:</strong> condominios /<strong> ação:</strong> delete-condominio -> PARAMS: <small>id (int), _csrf (token)</small></p>
          <p><strong>objeto:</strong> conselho /<strong> ação:</strong> delete-conselheiro -> PARAMS: <small>id (int), _csrf (token)</small></p>
          <p><strong>objeto:</strong> blocos /<strong> ação:</strong> delete-bloco -> PARAMS: <small>id (int), _csrf (token)</small></p>
          <p><strong>objeto:</strong> moradores /<strong> ação:</strong> delete-morador -> PARAMS: <small>id (int), _csrf (token)</small></p>
          <p><strong>objeto:</strong> unidades /<strong> ação:</strong> delete-unidade -> PARAMS: <small>id (int), _csrf (token)</small></p>
        </div>
      </div>
    </div>
</div>