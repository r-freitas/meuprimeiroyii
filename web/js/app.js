$(function () {


    $(document).on('change', '#id_condominio', function () {
        var selecionado = $(this).val();

        $.ajax({
            url: '?r=blocos/lista-bloco-api',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado },
            success: function (data) {
                selectPopulation('.fromBloco', data, 'nome');
            }
        })

    })

    //chamar unidades após selecionar blocos
    $(document).on('change', '.fromBloco', function () {
        var selecionado = $(this).val();

        $.ajax({
            url: '?r=unidades/lista-unidade-api',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado },
            success: function (data) {
                selectPopulation('.fromUnidade', data, 'numeroUnidade');
            }
        })

    })

    function selectPopulation(seletor, dados, field) {
        estrutura = '<option value="">Selecione...</option>';

        // if(dados.length > 1){
        for (let i = 0; i < dados.length; i++) {
            estrutura += '<option value="' + dados[i].id + '">' + dados[i][field] + '</option>';
        }
        // }else{
        //     estrutura += '<option value"'+dados.id+'">'+dados.nomeBloco+'</option>';
        // }
        $(seletor).html(estrutura);
    }

    //controle do modal
    $('.openModal').click(function () {
        caminho = $(this).attr('href');
        $('.modal-body').load(caminho, function (response, status) {
            if (status == "success") {
                $('#modalComponent').modal({ show: true });
            }
        });
        return false;
    })

})

function myAlert(tipo, mensagem, pai, url) {
    url = (url == undefined) ? url = '' : url = url;
    componente = '<div class="alert alert-' + tipo + '" role="alert">' + mensagem + '</div>';
    $(pai).prepend(componente);
    setTimeout(function () {
        $(pai).find('div.alert').remove();
        if (tipo == 'success' && url) {
            setTimeout(function () {
                window.location.href = url;
            }, 500)
        }
    }, 3000)

}

// Mascaras
$('input[name=cpf]').mask('000.000.000-00');
$('input[name=cep]').mask('00000-000');
$('input[name=cnpj]').mask('00.000.000/0000-00');

// Mascaras para o modal
$(document).on('click','input[name=cpf]', function(){
    $(this).mask('000.000.000-00');
})
$(document).on('click','input[name=cep]',function(){
    $(this).mask('00000-000');
})
$(document).on('click','input[name=cnpj]',function(){
    $(this).mask('00.000.000/0000-00');
})

var SPMaskBehavior = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
    spOptions = {
        onKeyPress: function (val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };

$(document).on('focus','input[name=telefone]',function(){
    $(this).mask(SPMaskBehavior, spOptions);
});

$(document).on('submit', '.formAdministradora, .formCondominio, .formConselho, .formMorador', function(){
    $('input[name=cpf]').unmask();
    $('input[name=cep]').unmask();
    $('input[name=cnpj]').unmask();
    $('input[name=telefone]').unmask();
});

// Busca por Cep
$(document).ready(function () {

    function limpa_formulário_cep() {
        // Limpa valores do formulário de cep.
        $("#cep").val("");
        $("#rua").val("");
        $("#bairro").val("");
        $("#cidade").val("");
        $("#uf").val("");
    }

    //Quando o campo cep perde o foco.
    $(document).on('blur', '#cep', function () {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if (validacep.test(cep)) {

                //Preenche os campos com "Carregando..." enquanto consulta webservice.
                $("#rua").val("Carregando...");
                $("#bairro").val("Carregando...");
                $("#cidade").val("Carregando...");
                $("#uf").val("Carregando...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#cep").val(dados.cep);
                        $("#rua").val(dados.logradouro);
                        $("#bairro").val(dados.bairro);
                        $("#cidade").val(dados.localidade);
                        $("#uf").val(dados.uf);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    });
});