<?php

namespace app\controllers;

use app\models\LoginForm;
use Yii;
use yii\web\Controller;
use app\models\MoradoresModel;
use app\models\UnidadesModel;
use app\models\BlocosModel;
use app\models\CondominiosModel;
use app\models\AdministradorasModel;
use app\models\ConselhoModel;
use app\models\User;

class SiteController extends Controller{

    public function actions(){
        return[
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ]
        ];
    }
    

    public function actionIndex(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        $moradores = MoradoresModel::find()->count();
        $condondominios = CondominiosModel::find()->count();
        $blocos = BlocosModel::find()->count();
        $unidades = UnidadesModel::find()->count();
        $administradoras = AdministradorasModel::find()->count();
        $usuarios = User::find()->count();
        $conselheiros = ConselhoModel::find()->count();

        $moradoresGrafico = \yii::$app->db->createCommand('
        SELECT
        COUNT(id) AS hoje,
        (SELECT COUNT(id) FROM ap_morador WHERE DATE(dataCadastro) = (CURDATE() - 1)) AS dMenosUm,
        (SELECT COUNT(id) FROM ap_morador WHERE DATE(dataCadastro) = (CURDATE() - 2)) AS dMenosDois,
        (SELECT COUNT(id) FROM ap_morador WHERE DATE(dataCadastro) = (CURDATE() - 3)) AS dMenosTres,
        (SELECT COUNT(id) FROM ap_morador WHERE DATE(dataCadastro) = (CURDATE() - 4)) AS dMenosQuatro
        FROM ap_morador
        WHERE DATE(dataCadastro) = CURDATE()
        ')->queryAll();

        $condominiosGrafico = \yii::$app->db->createCommand('
        SELECT
        COUNT(id) AS hoje,
        (SELECT COUNT(id) FROM ap_condominio WHERE DATE(dataCadastro) = (CURDATE() - 1)) AS dMenosUm,
        (SELECT COUNT(id) FROM ap_condominio WHERE DATE(dataCadastro) = (CURDATE() - 2)) AS dMenosDois,
        (SELECT COUNT(id) FROM ap_condominio WHERE DATE(dataCadastro) = (CURDATE() - 3)) AS dMenosTres,
        (SELECT COUNT(id) FROM ap_condominio WHERE DATE(dataCadastro) = (CURDATE() - 4)) AS dMenosQuatro
        FROM ap_condominio
        WHERE DATE(dataCadastro) = CURDATE()
        ')->queryAll();

        $administradorasGrafico = \yii::$app->db->createCommand('
        SELECT
        COUNT(id) AS hoje,
        (SELECT COUNT(id) FROM ap_administradora WHERE DATE(dataCadastro) = (CURDATE() - 1)) AS dMenosUm,
        (SELECT COUNT(id) FROM ap_administradora WHERE DATE(dataCadastro) = (CURDATE() - 2)) AS dMenosDois,
        (SELECT COUNT(id) FROM ap_administradora WHERE DATE(dataCadastro) = (CURDATE() - 3)) AS dMenosTres,
        (SELECT COUNT(id) FROM ap_administradora WHERE DATE(dataCadastro) = (CURDATE() - 4)) AS dMenosQuatro
        FROM ap_administradora
        WHERE DATE(dataCadastro) = CURDATE()
        ')->queryAll();

        $dia = date('Y-m-d');
        for ($i=0; $i < 5; $i++) { 
            $ultimosDias[$i] = date( 'd/m', strtotime( $dia . ' -'.$i.' day' ) );
        }

        return $this->render('home',[
            'moradores' => $moradores,
            'condominios' => $condondominios,
            'blocos' => $blocos,
            'unidades' => $unidades,
            'administradoras' => $administradoras,
            'usuarios' => $usuarios,
            'conselheiros' => $conselheiros,
            'ultimosDias' => $ultimosDias,
            'moradoresGrafico' => $moradoresGrafico[0],
            'condominiosGrafico' => $condominiosGrafico[0],
            'administradorasGrafico' => $administradorasGrafico[0]
        ]);
    }

    public function actionLogin(){
        $this->layout = false;
        if(!Yii::$app->user->isGuest){
            return $this->goHome();
        }

        $request = \yii::$app->request;

        if($request->isPost){
            $identity = LoginForm::findOne(['usuario' => $request->post('usuario'), 'senha' => $request->post('senha')]);
            //$identity = User::findOne(['usuario' => $request->post('usuario')]);
            if($identity){
                Yii::$app->user->login($identity);
                return $this->redirect(['index']);
            }else{
                return $this->redirect(['login', 'myAlert'=>['type' => 'warning', 'msg' => 'Os dados não conferem.', 'redir' => 'index.php?r=site/login']]);
            }
        }
        return $this->render('login');
    }

    public function actionLogout(){
        Yii::$app->user->logout();
        return $this->redirect((['site/login']));
    }
    
    
    // public function actionCadastrarAdministradoras(){
    //     return $this->render('cadastrar-administradoras');
    // }
    
    // public function actionListarAdministradoras(){
    //     return $this->render('listar-administradoras');
    // }
    
    // public function actionCadastrarCondominios(){
    //     return $this->render('cadastrar-condominios');
    // }

    // public function actionListarCondominios(){
    //     return $this->render('listar-blocos');
    // }

    // public function actionCadastrarBlocos(){
    //     return $this->render('cadastrar-blocos');
    // }

    // public function actionListarBlocos(){
    //     return $this->render('listar-blocos');
    // }

    // public function actionCadastrarUnidades(){
    //     return $this->render('cadastrar-unidades');
    // }

    // public function actionListarUnidades(){
    //     return $this->render('listar-unidades');
    // }

    // public function actionCadastrarMoradores(){
    //     return $this->render('cadastrar-moradores');
    // }

    // public function actionListarMoradores(){
    //     return $this->render('listar-moradores');
    // }

    // public function actionCadastrarConselheiros(){
    //     return $this->render('cadastrar-conselheiros');
    // }

    // public function actionListarConselheiros(){
    //     return $this->render('listar-conselheiros');
    // }

}
