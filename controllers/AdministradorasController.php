<?

namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\AdministradorasModel;
use Yii;

class AdministradorasController extends Controller{
    public function actionListarAdministradoras(){
 
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        $query = AdministradorasModel::find();

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $administradoras = $query->orderBy('nome')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();

        return $this->render('listar-administradoras',[
            'administradoras' => $administradoras,
            'paginacao' =>$paginacao,
        ]);
    }

    public static function listaAdministradoraSelect(){
        $query = AdministradorasModel::find();

        return $query->orderBy('nome')->all();
    }

    public function actionCadastrarAdministradoras(){
        
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        return $this->render('cadastrar-administradoras');

    }

    public function actionRealizaCadastroAdministradora(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new AdministradorasModel();
            $model->attributes = $request->post();
            $model->cnpj = str_replace('.','', $model->cnpj);
            $model->cnpj = str_replace('-','', $model->cnpj);
            $model->cnpj = str_replace('/','', $model->cnpj);
            $model->save();
            return $this->redirect(['administradoras/listar-administradoras']);
        }

        return $this->render('cadastrar-administradoras');
    }

    public function actionEditarAdministradora(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = AdministradorasModel::find();
            $administradoras = $query->where(['id' => $request->get()])->one();
        }
      
        return $this->render('editar-administradora',[
            'edit' => $administradoras
        ]);
    }

    public function actionRealizaEdicaoAdministradora(){
        $request = \yii::$app->request;
        if($request->isPost){
            $model = AdministradorasModel::findOne($request->post('id'));
            $model->attributes = $request->post();
            $model->cnpj = str_replace('.','', $model->cnpj);
            $model->cnpj = str_replace('-','', $model->cnpj);
            $model->cnpj = str_replace('/','', $model->cnpj);
            if($model->update()){
                return $this->redirect(['administradoras/listar-administradoras']);
            }else{
                return $this->redirect(['administradoras/listar-administradoras', 'msg' => 'erro']);
            }
        }
    }

    public function actionDeletaAdministradora(){
        $request = \yii::$app->request;
        if($request->isGet){
            $model = AdministradorasModel::findOne($request->get('id'));
            if($model->delete()){
                return $this->redirect(['administradoras/listar-administradoras', 'myAlert'=>['type' => 'success', 'msg'=>'Registro deletado com sucesso.']]);
            }else{
                return $this->redirect(['administradoras/listar-administradoras','myAlert'=>['type' => 'danger','msg'=>'Registro não deletado.']]);
            }
        }
    }


}

?>