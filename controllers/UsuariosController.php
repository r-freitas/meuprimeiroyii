<?

namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\User;
use Yii;

class UsuariosController extends Controller{
    public function actionListarUsuarios(){
 
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        $query = User::find();

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $usuarios = $query->orderBy('nome')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();

        return $this->render('listar-usuarios',[
            'usuarios' => $usuarios,
            'paginacao' =>$paginacao,
        ]);
    }

    public function actionCadastrarUsuarios(){
        
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        return $this->render('cadastrar-usuarios');

    }

    public function actionRealizaCadastroUsuario(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new User();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['usuarios/listar-usuarios']);
        }

        return $this->render('cadastrar-usuarios');
    }

    public function actionEditarUsuario(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = User::find();
            $usuarios = $query->where(['id' => $request->get()])->one();
        }
      
        return $this->render('editar-usuario',[
            'edit' => $usuarios
        ]);
    }

    public function actionRealizaEdicaoUsuario(){
        $request = \yii::$app->request;
        if($request->isPost){
            $model = User::findOne($request->post('id'));
            $model->attributes = $request->post();
            if($model->update()){
                return $this->redirect(['usuarios/listar-usuarios']);
            }else{
                return $this->redirect(['usuarios/listar-usuarios', 'msg' => 'erro']);
            }
        }
    }

    public function actionDeletaUsuario(){
        $request = \yii::$app->request;
        if($request->isGet){
            $model = User::findOne($request->get('id'));
            if($model->delete()){
                return $this->redirect(['usuarios/listar-usuarios', 'myAlert'=>['type' => 'success', 'msg'=>'Registro deletado com sucesso.']]);
            }else{
                return $this->redirect(['usuarios/listar-usuarios','myAlert'=>['type' => 'danger','msg'=>'Registro não deletado.']]);
            }
        }
    }


}

?>