<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\MoradoresModel;
use app\models\UnidadesModel;
use app\models\BlocosModel;
use app\models\CondominiosModel;
use Yii;

class MoradoresController extends Controller{
    public function actionListarMoradores(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        
        $moradorTable = MoradoresModel::tableName();
        $condTable = CondominiosModel::tableName();
        $blocoTable = BlocosModel::tableName();
        $unidadesTable = UnidadesModel::tableName();
        $query = (new \yii\db\Query())
            ->select(
                'morador.id,
                morador.id_bloco,
                morador.id_condominio,
                morador.id_unidade,
                bloco.nome AS nomeBloco,
                cond.nome AS nomeCondo,
                unidade.numeroUnidade AS numeroUnidade,
                morador.nome,
                morador.cpf,
                morador.email,
                morador.telefone,
                morador.dataCadastro,
                morador.dataModificacao'
            )
            ->from($moradorTable.' morador')
            ->innerJoin($condTable.' cond', 'morador.id_condominio = cond.id')
            ->innerJoin($blocoTable.' bloco', 'morador.id_bloco = bloco.id')
            ->innerJoin($unidadesTable.' unidade', 'morador.id_unidade = unidade.id');
        //$query = MoradoresModel::find();

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count()
        ]);

        $moradores = $query->orderBy('id')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();

        return $this->render('listar-moradores',[
            'moradores' => $moradores,
            'paginacao' => $paginacao
        ]);
    }

    public function actionCadastrarMoradores(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        return $this->render('cadastrar-moradores');
    }

    public function actionRealizaCadastroMorador(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new MoradoresModel();
            $model->attributes = $request->Post();
            $model->cpf = str_replace('.','', $model->cpf);
            $model->cpf = str_replace('-','', $model->cpf);
            $model->telefone = str_replace('-','', $model->telefone);
            $model->telefone = str_replace('(','', $model->telefone);
            $model->telefone = str_replace(')','', $model->telefone);
            $model->telefone = str_replace(' ','', $model->telefone);
            $model->save();
            return $this->redirect(['moradores/listar-moradores']);
        }
        return $this->render('cadastrar-moradores');
    }

    public function actionEditarMorador(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = MoradoresModel::find();
            $moradores = $query->where(['id' => $request->get()])->one();
        }
        // print_r($moradores);
        
        
        return $this->render('editar-morador',[
            'edit' => $moradores
        ]);
    }

    public function actionRealizaEdicaoMorador(){
        $request = \yii::$app->request;
        if($request->isPost){
            $model = MoradoresModel::findOne($request->post('id'));
            $model->attributes = $request->post();
            $model->cpf = str_replace('.','', $model->cpf);
            $model->cpf = str_replace('-','', $model->cpf);
            $model->telefone = str_replace('-','', $model->telefone);
            $model->telefone = str_replace('(','', $model->telefone);
            $model->telefone = str_replace(')','', $model->telefone);
            $model->telefone = str_replace(' ','', $model->telefone);
            if($model->update()){
                return $this->redirect(['moradores/listar-moradores']);
            }else{
                return $this->redirect(['moradores/listar-moradores', 'msg' => 'erro']);
            }
        }
    }

    public function actionDeletaMorador(){
        $request = \yii::$app->request;

        if($request->isGet){
            $model = MoradoresModel::findOne($request->get('id'));
            if($model->delete()){
                return $this->redirect(['moradores/listar-moradores', 'myAlert'=>['type' => 'success', 'msg'=>'Registro deletado com sucesso.']]);
            }else{
                return $this->redirect(['moradores/listar-moradores','myAlert'=>['type' => 'danger','msg'=>'Registro não deletado.']]);
            }
        }
    }

}


?>