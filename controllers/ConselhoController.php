<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\ConselhoModel;
use app\models\CondominiosModel;
use Yii;

class ConselhoController extends Controller{
    public function actionListarConselheiros(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        $condTable = CondominiosModel::tableName();
        $conselhoTable = ConselhoModel::tableName();
        $query = (new \yii\db\Query())
            ->select(
                'conselho.id,
                cond.nome AS nomeCondo,
                conselho.nomeConselho,
                conselho.funcao,
                conselho.cpf,
                conselho.email,
                conselho.telefone,
                conselho.dataCadastro,
                conselho.dataModificado'
            )
            ->from($conselhoTable.' conselho')
            ->innerJoin($condTable.' cond', 'conselho.id_condominio = cond.id');

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count()
        ]);

        $conselheiros = $query->orderBy('id')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();

        return $this->render('listar-conselheiros',[
            'conselheiros' => $conselheiros,
            'paginacao' => $paginacao
        ]);
    }

    public function actionCadastrarConselheiros(){
        
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        return $this->render('cadastrar-conselheiros');
    }

    public function actionRealizaCadastroConselheiro(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new ConselhoModel();
            $model->attributes = $request->post();
            $model->cpf = str_replace('.','', $model->cpf);
            $model->cpf = str_replace('-','', $model->cpf);
            $model->telefone = str_replace('-','', $model->telefone);
            $model->telefone = str_replace('(','', $model->telefone);
            $model->telefone = str_replace(')','', $model->telefone);
            $model->telefone = str_replace(' ','', $model->telefone);
            $model->save();
            return $this->redirect(['conselho/listar-conselheiros']);
        }

        return $this->render('cadastrar-conselheiros');
    }

    public function actionEditarConselheiro(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = ConselhoModel::find();
            $conselho = $query->where(['id' => $request->get()])->one();
        }
      
        return $this->render('editar-conselheiro',[
            'edit' => $conselho
        ]);
    }

    public function actionRealizaEdicaoConselheiro(){
        $request = \yii::$app->request;
        if($request->isPost){
            $model = ConselhoModel::findOne($request->post('id'));
            $model->attributes = $request->post();
            $model->cpf = str_replace('.','', $model->cpf);
            $model->cpf = str_replace('-','', $model->cpf);
            $model->telefone = str_replace('-','', $model->telefone);
            $model->telefone = str_replace('(','', $model->telefone);
            $model->telefone = str_replace(')','', $model->telefone);
            $model->telefone = str_replace(' ','', $model->telefone);
            if($model->update()){
                return $this->redirect(['conselho/listar-conselheiros']);
            }else{
                return $this->redirect(['conselho/listar-conselheiros', 'msg' => 'erro']);
            }
        }
    }

    
    public function actionDeletaConselheiro(){
        $request = \yii::$app->request;

        if($request->isGet){
            $model = ConselhoModel::findOne($request->get('id'));
            if($model->delete()){
                return $this->redirect(['conselho/listar-conselheiros', 'myAlert'=>['type' => 'success', 'msg'=>'Registro deletado com sucesso.']]);
            }else{
                return $this->redirect(['conselho/listar-conselheiros','myAlert'=>['type' => 'danger','msg'=>'Registro não deletado.']]);
            }
        }
    }


}

?>