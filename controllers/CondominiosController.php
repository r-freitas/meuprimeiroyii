<?

namespace app\controllers;

use app\models\AdministradorasModel;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\CondominiosModel;
use app\controllers\userLogadoController;
use Yii;

class CondominiosController extends Controller{
    
    public function actionListarCondominios(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        $condTable = CondominiosModel::tableName();
        $admTable = AdministradorasModel::tableName();
        // $query = CondominiosModel::find();
        $query = (new \yii\db\Query())
            ->select(
                'cond.id,
                cond.id_administradora, 
                adm.nome AS admNome,
                cond.nome,
                cond.qtBlocos,
                cond.cep,
                cond.logradouro,
                cond.numero,
                cond.bairro,
                cond.cidade,
                cond.estado,
                cond.dataCadastro,
                cond.dataModificado'
            )
            ->from($condTable.' cond')
            ->innerJoin($admTable.' adm', 'adm.id = cond.id_administradora');

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $condominios = $query->orderBy('cond.nome')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();

        return $this->render('listar-condominios',[
            'condominios' => $condominios,
            'paginacao' =>$paginacao,
        ]);
    }

    public static function listaCondominioSelect(){
        $query = CondominiosModel::find();

        return $query->orderBy('nome')->all();
    }

    public function actionCadastrarCondominios(){
        
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        return $this->render('cadastrar-condominios');

    }

    public function actionRealizaCadastroCondominio(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new CondominiosModel();
            $model->attributes = $request->post();
            $model->cep = str_replace('-','', $model->cep);
            $model->save();
            return $this->redirect(['condominios/listar-condominios']);
        }

        return $this->render('cadastrar-condominios');
    }

    public function actionEditarCondominio(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = CondominiosModel::find();
            $condominios = $query->where(['id' => $request->get()])->one();
        }
      
        return $this->render('editar-condominio',[
            'edit' => $condominios
        ]);
    }

    public function actionRealizaEdicaoCondominio(){
        $request = \yii::$app->request;
        if($request->isPost){
            $model = CondominiosModel::findOne($request->post('id'));
            $model->attributes = $request->post();
            $model->cep = str_replace('-','', $model->cep);
            if($model->update()){
                return $this->redirect(['condominios/listar-condominios']);
            }else{
                return $this->redirect(['condominios/listar-condominios', 'msg' => 'erro']);
            }
        }
    }

    public function actionDeletaCondominio(){
        $request = \yii::$app->request;

        if($request->isGet){
            $model = CondominiosModel::findOne($request->get('id'));
            if($model->delete()){
                return $this->redirect(['condominios/listar-condominios', 'myAlert'=>['type' => 'success', 'msg'=>'Registro deletado com sucesso.']]);
            }else{
                return $this->redirect(['condominios/listar-condominios','myAlert'=>['type' => 'danger','msg'=>'Registro não deletado.']]);
            }
        }
    }

}

?>