<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\UnidadesModel;
use app\models\BlocosModel;
use app\models\CondominiosModel;
use Yii;

class UnidadesController extends Controller{
    public function actionListarUnidades(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        $condTable = CondominiosModel::tableName();
        $blocoTable = BlocosModel::tableName();
        $unidadesTable = UnidadesModel::tableName();
        $query = (new \yii\db\Query())
            ->select(
                'unidade.id,
                unidade.id_bloco,
                bloco.nome AS nomeBloco,
                cond.nome AS nomeCondo,
                unidade.numeroUnidade,
                unidade.metragem,
                unidade.vagasDeGaragem,
                unidade.dataCadastro,
                unidade.dataModificado'
            )
            ->from($unidadesTable.' unidade')
            ->innerJoin($condTable.' cond', 'unidade.id_condominio = cond.id')
            ->innerJoin($blocoTable.' bloco', 'unidade.id_bloco = bloco.id');
        //$query = UnidadesModel::find();

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count()
        ]);

        $unidades = $query->orderBy('numeroUnidade')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();

        return $this->render('listar-unidades',[
            'unidades' => $unidades,
            'paginacao' => $paginacao
        ]);
    }

    public static function listaUnidadeSelect(){
        $query = UnidadesModel::find();

        return $query->orderBy('numeroUnidade')->all();
    }

    public static function listaUnidadeEditar($id_bloco){
        
        $query = UnidadesModel::find();
        $data = $query->where(['id_bloco' => $id_bloco])->orderBy('numeroUnidade')->all();
        return $data;
    }

    public function actionCadastrarUnidades(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        return $this->render('cadastrar-unidades');
    }

    public function actionListaUnidadeApi(){
        $request = \yii::$app->request;
        $query = UnidadesModel::find();
        $data = $query->where(['id_bloco' => $request->post()])->orderBy('numeroUnidade')->all();

        $dados = array();
        $i = 0;
        foreach($data as $d){
            $dados[$i]['id'] = $d['id'];
            $dados[$i]['numeroUnidade'] = $d['numeroUnidade'];
            $i++;
        }
        return json_encode($dados);
    }

    public function actionRealizaCadastroUnidade(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new UnidadesModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['unidades/listar-unidades']);
        }
        return $this->render('cadastrar-condominios');
    }

    public function actionEditarUnidade(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = UnidadesModel::find();
            $unidades = $query->where(['id' => $request->get()])->one();
        }        
        
        return $this->render('editar-unidade',[
            'edit' => $unidades
        ]);
    }

    public function actionRealizaEdicaoUnidade(){
        $request = \yii::$app->request;
        if($request->isPost){
            $model = UnidadesModel::findOne($request->post('id'));
            $model->attributes = $request->post();
            if($model->update()){
                return $this->redirect(['unidades/listar-unidades']);
            }else{
                return $this->redirect(['unidades/listar-unidades', 'msg' => 'erro']);
            }
        }
    }

    public function actionDeletaUnidade(){
        $request = \yii::$app->request;

        if($request->isGet){
            $model = UnidadesModel::findOne($request->get('id'));
            if($model->delete()){
                return $this->redirect(['unidades/listar-unidades', 'myAlert'=>['type' => 'success', 'msg'=>'Registro deletado com sucesso.']]);
            }else{
                return $this->redirect(['unidades/listar-unidades','myAlert'=>['type' => 'danger','msg'=>'Registro não deletado.']]);
            }
        }
    }

}

?>