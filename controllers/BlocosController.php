<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\BlocosModel;
use app\models\CondominiosModel;
use Yii;

class BlocosController extends Controller{
    public function actionListarBlocos(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        $condTable = CondominiosModel::tableName();
        $blocoTable = BlocosModel::tableName();
        $query = (new \yii\db\Query())
            ->select(
                'bloco.id,
                bloco.id_condominio,
                cond.nome AS nomeCond,
                bloco.nome,
                bloco.numeroAndares,
                bloco.unidadesPAndar,
                bloco.dataCadastro,
                bloco.dataModificado'
            )
            ->from($blocoTable.' bloco')
            ->innerJoin($condTable.' cond', 'bloco.id_condominio = cond.id');
        //$query = BlocosModel::find();

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count()
        ]);

        $blocos = $query->orderBy('nome')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();

        return $this->render('listar-blocos',[
            'blocos' => $blocos,
            'paginacao' => $paginacao
        ]);
    }

    public static function listaBlocoSelect(){
        $query = BlocosModel::find();

        return $query->orderBy('nome')->all();
    }

    public static function listaBlocoEditar($id_condominio){
        $query = BlocosModel::find();
        $data = $query->where(['id_condominio' =>$id_condominio])->orderBy('nome')->all();
        return $data;
    }

    public function actionListaBlocoApi(){
        $request = \yii::$app->request;
        $query = BlocosModel::find();
        $data = $query->where(['id_condominio' => $request->post()])->orderBy('nome')->all();

        $dados = array();
        $i = 0;
        foreach($data as $d){
            $dados[$i]['id'] = $d['id'];
            $dados[$i]['nome'] = $d['nome'];
            $i++;
        }
        return json_encode($dados);
    }

    public function actionCadastrarBlocos(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        
        return $this->render('cadastrar-blocos');
    }

    public function actionRealizaCadastroBloco(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new BlocosModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['blocos/listar-blocos']);
        }

        return $this->render('cadastrar-blocos');
    }

    public function actionEditarBloco(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = BlocosModel::find();
            $blocos = $query->where(['id' => $request->get()])->one();
        }
      
        return $this->render('editar-bloco',[
            'edit' => $blocos
        ]);
    }

    public function actionRealizaEdicaoBloco(){
        $request = \yii::$app->request;
        if($request->isPost){
            $model = BlocosModel::findOne($request->post('id'));
            $model->attributes = $request->post();
            if($model->update()){
                return $this->redirect(['blocos/listar-blocos']);
            }else{
                return $this->redirect(['blocos/listar-blocos', 'msg' => 'erro']);
            }
        }
    }

    public function actionDeletaBloco(){
        $request = \yii::$app->request;

        if($request->isGet){
            $model = BlocosModel::findOne($request->get('id'));
            if($model->delete()){
                return $this->redirect(['blocos/listar-blocos', 'myAlert'=>['type' => 'success', 'msg'=>'Registro deletado com sucesso.']]);
            }else{
                return $this->redirect(['blocos/listar-blocos','myAlert'=>['type' => 'danger','msg'=>'Registro não deletado.']]);
            }
        }
    }

    
}


?>