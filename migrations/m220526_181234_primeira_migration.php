<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class m220526_181234_primeira_migration
 */
class m220526_181234_primeira_migration extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ap_veiculos', [
            'id' => Schema::TYPE_PK,
            'modelo' => Schema::TYPE_STRING . ' NOT NULL',
            'placa' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
        
        $this->insert('ap_veiculos', [
            'modelo' => 'Kombi',
            'placa' => 'IJB-3265',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ap_pets');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220526_181234_primeira_migration cannot be reverted.\n";

        return false;
    }
    */
}
