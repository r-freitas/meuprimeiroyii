<?php

use app\components\estadosComponent;
use yii\db\Migration;
use yii\db\mysql\Schema;
use yii\rest\UpdateAction;

/**
 * Class m220526_191145_segunda_migration
 */
class m220526_191145_segunda_migration extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        
        $this->createTable('ap_administradoras', [
            'id' => $this->primaryKey(),
            'nome' => $this->string(255)->notNull(),
            'cnpj' => $this->string(14)->notNull(),
            'dataCadastro' => $this->timestamp(),
            'dataModificacao' => $this->timestamp()
        ]);

        
        $this->createTable('ap_condominios', [
            'id' => $this->primaryKey(),
            'id_administradora' => $this->integer()->notNull(),
            'nome' => $this->string(255),
            'qtBlocos' => $this->integer(3),
            'cep' => $this->string(8),
            'logradouro' => $this->string(255),
            'numero' => $this->string(8),
            'bairro' => $this->string(255),
            'cidade' => $this->string(255),
            'estado' => $this->string(2),
            'dataCadastro' => $this->timestamp(),
            'dataModificacao' => $this->timestamp()
        ]);
        
        $this->addForeignKey(
            'fk-ap_condominios-id_administradora',
            'ap_condominios',
            'id_administradora',
            'ap_administradoras',
            'id'
        );

                
        $this->createTable('ap_blocos', [
            'id' => $this->primaryKey(),
            'id_condominio' => $this->integer()->notNull(),
            'nome' => $this->string(255),
            'numeroAndares' => $this->integer(3),
            'unidadesPAndar' => $this->integer(3),
            'dataCadastro' => $this->timestamp(),
            'dataModificacao' => $this->timestamp()
        ]);
        
        $this->addForeignKey(
            'fk-ap_blocos-id_condominio',
            'ap_blocos',
            'id_condominio',
            'ap_condominios',
            'id'
        );

        $this->createTable('ap_unidades', [
            'id' => $this->primaryKey(),
            'id_condominio' => $this->integer()->notNull(),
            'id_bloco' => $this->integer()->notNull(),
            'numeroUnidade' => $this->string(255),
            'metragem' => $this->float(),
            'vagasDeGaragem' => $this->integer(2),
            'dataCadastro' => $this->timestamp(),
            'dataModificacao' => $this->timestamp()
        ]);
        
        $this->addForeignKey(
            'fk-ap_unidades-id_condominio',
            'ap_unidades',
            'id_condominio',
            'ap_condominios',
            'id'
        );

        $this->addForeignKey(
            'fk-ap_unidades-id_bloco',
            'ap_unidades',
            'id_bloco',
            'ap_blocos',
            'id'
        );


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey('fk-ap_unidades-id_condominio', 'ap_unidades');
        $this->dropForeignKey('fk-ap_unidades-id_bloco', 'ap_unidades');
        $this->dropTable('ap_unidades');

        $this->dropForeignKey('fk-ap_blocos-id_condominio', 'ap_blocos');
        $this->dropTable('ap_blocos');

        $this->dropForeignKey('fk-ap_condominios-id_administradora', 'ap_condominios');
        $this->dropTable('ap_condominios');

        $this->dropTable('ap_administradoras');

        //return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220526_191145_segunda_migration cannot be reverted.\n";

        return false;
    }
    */
}
