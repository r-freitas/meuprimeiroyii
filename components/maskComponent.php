<?
namespace app\components;

use yii\base\Component;

Class maskComponent extends Component{

    public static function mask($val = '',$format){
        $maskared = '';
        $k=0;
        switch($format){
            case 'cpf':
                $mask = '###.###.###-##';
                break;
            case 'cnpj':
                $mask = '##.###.###/####-##';
                break;
            case 'cep':
                $mask = '#####-###';
                break;
            case 'telefone':
                $mask = (strlen($val) > 10 ? '## #####-####' : '## ####-####');
                break;
            default:
                $mask = null;
                break;
        }
        if($val == null){
            return "--";
        }
        for($i = 0; $i <=strlen($mask) -1; $i++){
            if($mask[$i] == '#'){
                if(isset($val[$k])){
                    $maskared .= $val[$k++];
                }
            }else if(isset($mask[$i])){
                $maskared .= $mask[$i];
            }
        }

        return $maskared;
        
    }
    
    public static function cpfMask($d){
        //000.000.000-00
        $str1 = substr($d,0,3).'.';
        $str2 = substr($d,3,3).'.';
        $str3 = substr($d,6,3).'-';
        $str4 = substr($d,9,2);

        return $str1.$str2.$str3.$str4;
    }

    public static function cnpjMask($d){
        //00.000.000/0000-00
        $str1 = substr($d,0,2).'.';
        $str2 = substr($d,2,3).'.';
        $str3 = substr($d,5,3).'/';
        $str4 = substr($d,8,4).'-';
        $str5 = substr($d,12,2);

        return $str1.$str2.$str3.$str4.$str5;
    }

    public static function cepMask($d){
        //88888-888
        $str1 = substr($d,0,5).'-';
        $str2 = substr($d,5,8);

        return $str1.$str2;
    }

    public static function telefoneMask($d){
        //(00) 0000-0000
        $str1 = '('.substr($d,0,2).')';
        $str2 = substr($d,2,4).'-';
        $str3 = substr($d,6,10);

        return $str1.$str2.$str3;
    }


}


?>