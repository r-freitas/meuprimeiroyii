-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.24-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para ap_controle
CREATE DATABASE IF NOT EXISTS `ap_controle` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `ap_controle`;

-- Copiando estrutura para tabela ap_controle.ap_administradora
CREATE TABLE IF NOT EXISTS `ap_administradora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cnpj` varchar(14) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  `dataModificacao` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.ap_administradora: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_administradora` DISABLE KEYS */;
INSERT INTO `ap_administradora` (`id`, `nome`, `cnpj`, `dataCadastro`, `dataModificacao`) VALUES
	(1, 'Avacon', '45648435000154', '2022-05-08 20:38:17', '2022-05-08 20:38:17'),
	(2, 'Biz Inteligência Imobiliária', '13465487000154', '2022-05-09 20:47:40', '2022-05-09 20:47:40'),
	(3, 'CAV', '45789458000166', '2022-05-10 20:48:03', '2022-05-10 20:48:03');
/*!40000 ALTER TABLE `ap_administradora` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.ap_bloco
CREATE TABLE IF NOT EXISTS `ap_bloco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_condominio` int(11) NOT NULL DEFAULT 0,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `numeroAndares` int(3) NOT NULL DEFAULT 0,
  `unidadesPAndar` int(3) NOT NULL DEFAULT 0,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  `dataModificado` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.ap_bloco: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_bloco` DISABLE KEYS */;
INSERT INTO `ap_bloco` (`id`, `id_condominio`, `nome`, `numeroAndares`, `unidadesPAndar`, `dataCadastro`, `dataModificado`) VALUES
	(1, 1, '5', 16, 6, '2022-05-09 21:08:51', NULL),
	(2, 2, '1', 7, 6, '2022-05-09 21:09:15', NULL),
	(3, 5, '8', 4, 16, '2022-05-09 21:09:33', NULL),
	(4, 1, '4', 16, 6, '2022-05-09 21:09:58', NULL),
	(5, 4, 'A', 10, 4, '2022-05-10 18:42:47', NULL),
	(6, 6, 'Ortência', 6, 4, '2022-05-10 18:43:08', NULL),
	(7, 3, 'Ilhas Canárias', 10, 2, '2022-05-10 18:46:09', NULL),
	(8, 7, '1', 18, 4, '2022-05-10 19:19:41', NULL);
/*!40000 ALTER TABLE `ap_bloco` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.ap_condominio
CREATE TABLE IF NOT EXISTS `ap_condominio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_administradora` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `qtBlocos` int(11) NOT NULL DEFAULT 0,
  `cep` varchar(8) NOT NULL DEFAULT '',
  `logradouro` varchar(255) NOT NULL DEFAULT '',
  `numero` varchar(8) NOT NULL DEFAULT '',
  `bairro` varchar(255) NOT NULL DEFAULT '',
  `cidade` varchar(255) NOT NULL DEFAULT '',
  `estado` varchar(255) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  `dataModificado` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.ap_condominio: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_condominio` DISABLE KEYS */;
INSERT INTO `ap_condominio` (`id`, `id_administradora`, `nome`, `qtBlocos`, `cep`, `logradouro`, `numero`, `bairro`, `cidade`, `estado`, `dataCadastro`, `dataModificado`) VALUES
	(1, 1, 'Residencial Rita Vieira', 5, '82600450', 'Rua Júlio Augusto Ansiutti', '456', 'Bacacheri', 'Curitiba', 'PR', '2022-05-06 20:50:36', NULL),
	(2, 1, 'Recanto das Montanhas', 1, '97110290', 'Rua Floresta', '212', 'Camobi', 'Santa Maria', 'RS', '2022-05-07 20:51:31', NULL),
	(3, 2, 'Alphaville', 2, '89032400', 'Rua Johann G H Hadlich', '450', 'Passo Manso', 'Blumenau', 'SC', '2022-05-08 20:53:23', NULL),
	(4, 2, 'München', 2, '99470000', 'Av. Brasil', '250', 'Centro', 'Não-Me-Toque', 'RS', '2022-05-09 20:56:29', NULL),
	(5, 3, 'Castelo Luxemburgo', 10, '78200100', 'Rua dos Esteves e Lacerda', '456', 'Jardim Imperial', 'Cáceres', 'MT', '2022-05-09 20:57:17', NULL),
	(6, 3, 'Flórida', 1, '87400000', 'Av. Sete de Setembro', '789', 'Centro', 'Cruzeiro do Oeste', 'PR', '2022-05-10 20:59:28', NULL),
	(7, 3, 'Green Tower', 18, '79002190', 'Rua da Paz', '100', 'Centro', 'Campo Grande', 'MS', '2022-05-10 19:19:19', NULL);
/*!40000 ALTER TABLE `ap_condominio` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.ap_conselho
CREATE TABLE IF NOT EXISTS `ap_conselho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_condominio` int(11) NOT NULL DEFAULT 0,
  `funcao` enum('Sindico','Subsindico','Conselheiro') NOT NULL,
  `nomeConselho` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(11) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `telefone` varchar(15) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  `dataModificado` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.ap_conselho: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_conselho` DISABLE KEYS */;
INSERT INTO `ap_conselho` (`id`, `id_condominio`, `funcao`, `nomeConselho`, `cpf`, `email`, `telefone`, `dataCadastro`, `dataModificado`) VALUES
	(1, 1, 'Subsindico', 'Alexandre', '15648941894', 'alexandre@ritavieira.com', '54468744777', '2022-05-09 21:23:29', '2022-05-09 21:23:29');
/*!40000 ALTER TABLE `ap_conselho` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.ap_morador
CREATE TABLE IF NOT EXISTS `ap_morador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_unidade` int(11) NOT NULL DEFAULT 0,
  `id_bloco` int(11) NOT NULL DEFAULT 0,
  `id_condominio` int(11) NOT NULL DEFAULT 0,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(11) NOT NULL DEFAULT '',
  `email` varchar(150) NOT NULL DEFAULT '',
  `telefone` varchar(15) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `dataModificacao` timestamp NULL DEFAULT NULL,
  `nascimento` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.ap_morador: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_morador` DISABLE KEYS */;
INSERT INTO `ap_morador` (`id`, `id_unidade`, `id_bloco`, `id_condominio`, `nome`, `cpf`, `email`, `telefone`, `dataCadastro`, `dataModificacao`, `nascimento`) VALUES
	(1, 1, 1, 1, 'Reginaldo', '45678498779', 'reginaldo@email.com', '47984874857', '2022-05-09 21:16:57', NULL, '1990-01-01'),
	(2, 2, 1, 1, 'Bruno', '48948964896', 'bruno@emailmaislongo.com', '46578189874', '2022-05-09 21:17:35', NULL, '1990-02-02'),
	(3, 7, 2, 2, 'Reginaldo', '15489648949', 'reginaldo.freitas@apcoders.com.br', '47874864896', '2022-05-08 18:50:08', NULL, '1990-01-01'),
	(4, 8, 5, 4, 'Aline da Silva', '21654897418', 'alinedasilva@gmail.com', '48974894686', '2022-05-07 18:51:47', NULL, '1988-03-03'),
	(5, 3, 1, 1, 'Bruno Oliveira', '21654698679', 'brunooliveira@oliveragaragem.com', '45478746978', '2022-05-07 18:54:35', NULL, '1995-06-05'),
	(6, 9, 3, 5, 'Luis Gabriel', '12546848647', 'luisgabriel@apcoders.com.br', '47585486946', '2022-05-06 18:55:37', NULL, '1999-02-02'),
	(7, 5, 7, 3, 'Alanis Cibele', '35456441687', 'alaniscibele@apcoders.com.br', '47898985978', '2022-05-06 18:56:29', NULL, '2005-08-14'),
	(8, 6, 6, 6, 'Tiago Augusto', '15649867418', 'tiagoaugusto@volkswagen.com.br', '11957849648', '2022-05-06 18:58:38', NULL, '2000-11-04'),
	(9, 8, 5, 4, 'Vanderleia da Silva', '65465416854', 'leiavan@gmail.com', '47549849489', '2022-05-05 18:59:21', NULL, '1981-05-05'),
	(10, 7, 2, 2, 'Dirlei Santana das Santos', '65189418941', 'dirlei@gmail.com', '55665498449', '2022-05-08 19:00:17', NULL, '1950-05-05'),
	(11, 7, 2, 2, 'Nicole Clara Martins', '62738943020', 'nicole-martins70@gdsambiental.com.br', '68983972950', '2022-05-10 19:09:08', NULL, '0076-04-06'),
	(12, 5, 7, 3, 'Pietra Sophia Nogueira', '85268120506', 'pietra-nogueira80@gmail.com', '67982919737', '2022-05-10 19:10:09', NULL, '1985-01-16'),
	(13, 3, 1, 1, 'Luiza Mirella da Conceição', '44635464864', 'luiza.mirella.daconceicao@iclaud.com', '61999594239', '2022-05-10 19:12:15', NULL, '1995-12-02'),
	(14, 4, 1, 1, 'Benedito Martin Benedito Silva', '38708080744', 'benedito-silva94@emcinfo.com.br', '61992107684', '2022-05-10 19:13:15', NULL, '1958-05-07'),
	(15, 10, 8, 7, 'Guilherme Arrastaud', '16456165461', 'guilhermearrastaud@hotmail.com', '67898484888', '2022-05-10 19:20:41', NULL, '1990-05-08');
/*!40000 ALTER TABLE `ap_morador` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.ap_pets
CREATE TABLE IF NOT EXISTS `ap_pets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomePet` varchar(255) NOT NULL DEFAULT '',
  `tipo` enum('Cachorro','Gato','Passarinho') DEFAULT NULL,
  `id_morador` int(11) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.ap_pets: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_pets` DISABLE KEYS */;
INSERT INTO `ap_pets` (`id`, `nomePet`, `tipo`, `id_morador`, `dataCadastro`) VALUES
	(1, 'Lupe', 'Cachorro', 1, '2022-03-30 11:32:35'),
	(2, 'Tina', 'Cachorro', 1, '2022-03-30 11:32:52'),
	(3, 'Zeus', 'Cachorro', 2, '2022-03-30 11:33:07');
/*!40000 ALTER TABLE `ap_pets` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.ap_unidade
CREATE TABLE IF NOT EXISTS `ap_unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_bloco` int(11) NOT NULL DEFAULT 0,
  `id_condominio` int(11) NOT NULL DEFAULT 0,
  `numeroUnidade` varchar(5) NOT NULL DEFAULT '',
  `metragem` float NOT NULL DEFAULT 0,
  `vagasDeGaragem` int(2) NOT NULL DEFAULT 0,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  `dataModificado` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.ap_unidade: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_unidade` DISABLE KEYS */;
INSERT INTO `ap_unidade` (`id`, `id_bloco`, `id_condominio`, `numeroUnidade`, `metragem`, `vagasDeGaragem`, `dataCadastro`, `dataModificado`) VALUES
	(1, 1, 1, '405', 72, 2, '2022-05-09 21:11:09', NULL),
	(2, 1, 1, '404', 60, 1, '2022-05-09 21:11:29', NULL),
	(3, 1, 1, '403', 60, 1, '2022-05-09 21:11:47', NULL),
	(4, 1, 1, '402', 60, 1, '2022-05-09 21:12:33', NULL),
	(5, 7, 3, '11', 105, 3, '2022-05-10 18:48:40', NULL),
	(6, 6, 6, '202', 65, 1, '2022-05-10 18:49:01', NULL),
	(7, 2, 2, '705', 68, 2, '2022-05-10 18:49:31', NULL),
	(8, 5, 4, '102', 50, 1, '2022-05-10 18:51:08', NULL),
	(9, 3, 5, '301', 42, 1, '2022-05-10 18:55:00', NULL),
	(10, 8, 7, '801', 90, 2, '2022-05-10 19:19:59', NULL);
/*!40000 ALTER TABLE `ap_unidade` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.ap_usuario
CREATE TABLE IF NOT EXISTS `ap_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `usuario` varchar(50) NOT NULL DEFAULT '',
  `senha` varchar(255) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.ap_usuario: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_usuario` DISABLE KEYS */;
INSERT INTO `ap_usuario` (`id`, `nome`, `usuario`, `senha`, `dataCadastro`) VALUES
	(10, 'reginaldo', 'reginaldo', 'teste', '2022-04-24 19:32:55');
/*!40000 ALTER TABLE `ap_usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
